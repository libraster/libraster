/*
 * Copyright 2012-2013 James Cloos
 *
 *
 * This file is part of LibRaster.
 *
 * LibRaster is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * LibRaster is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with LibRaster.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "raster.h"
#include "raster-private.h"
#include "enum-parse-data.h"

static size_t
_raster_write_int (FILE *output, int32_t i)
{
	uint32_t j = htonl ((uint32_t)i);
	return fwrite (&j, sizeof(j), 1, output);
}

static size_t
_raster_write_uint (FILE *output, uint32_t i)
{
	uint32_t j = htonl (i);
	return fwrite (&j, sizeof(j), 1, output);
}

static size_t
_raster_write_reserved (FILE *output, int octets)
{
	unsigned char zero = 0;
	size_t count = 0;
	int i;

	for (i = 0; i < octets; i++) {
		count += fwrite (&zero, sizeof(zero), 1, output);
	}

	return count;
}

static size_t
_raster_write_string (FILE* output, const uint8_t *string)
{
	uint8_t buf[RASTER_STRING_WIDTH];

	memset (buf, 0, RASTER_STRING_WIDTH);
	memcpy (buf, string, strlen(string));

	return fwrite (buf, sizeof(uint8_t),
		RASTER_STRING_WIDTH, output);
}

static size_t
_raster_write_vendor_data (FILE* output, const uint8_t *vd, size_t len)
{
	uint8_t buf[RASTER_MAX_VENDOR_DATA];

	memset (buf, 0, RASTER_MAX_VENDOR_DATA);
	memcpy (buf, vd, len);

	return fwrite (buf, sizeof(uint8_t),
		RASTER_STRING_WIDTH, output);
}

static inline uint32_t
_clamp (uint32_t x, uint32_t a, uint32_t b)
{
	if (x > b)
		return b;

	if (x < a)
		return a;

	return x;

}

static inline uint32_t
_raster_srgb32 (uint32_t alpha, uint32_t red, uint32_t green, uint32_t blue)
{
	uint32_t ap;

	ap  =  _clamp (blue, 0, 0xFF);
	ap |= (_clamp (green,0, 0xFF) << 8);
	ap |= (_clamp (red,  0, 0xFF) << 16);
	ap |= (_clamp (alpha,0, 0xFF) << 24);
	
	return ap;
}

static inline void
_raster_split_srgb32 (uint8_t *alpha, uint8_t *red, uint8_t *green, uint8_t *blue, uint32_t ap)
{

	*blue  =  ap & 0xFF;
	*green = (ap & 0xFF00)     >> 8;
	*red   = (ap & 0xFF0000)   >> 16;
	*alpha = (ap & 0xFF000000) >> 24;
}

static uint32_t
_raster_octets_per_pixel (uint32_t bits_per_pixel)
{
	return (bits_per_pixel + 7) >> 3;
}

/* abbabbabb... => 0a1b0a1b0a1b... => 4/3 */
static size_t
_raster_longest_packed_line (uint32_t opp, uint32_t width)
{
	return 1 + opp * width * 4 / 3;
}

static uint32_t
_raster_octets_per_line (uint32_t bpp, uint32_t width)
{
	return _raster_octets_per_pixel (bpp) * width;
}

static size_t
_raster_pack_line (uint8_t *out, const uint8_t *in, size_t ilen, uint32_t octets)
{
	/* max octets/pixel is 30, for 16bit Device15 */
	uint8_t buf[4096];
	int len = 0, repeat = 0, end = 0;
	int i = 0, j = 0;

	memset (buf, 0, 4096);

	// len is a count of bundles
	while (!end) {
		end = (i == ilen);
		if (!end) {
			memcpy (buf+(len*octets), in+i, octets);
			i += octets;
			len++;
			if (len <= 1) continue;
		}
		
		if (repeat) {
			if (memcmp (buf + (len-1)*octets, buf + (len-2)*octets, octets))
				repeat = 0;
			if (!repeat || len == 129 || end) {
				/* write out repeating bundles */
				out[j++] = end ? len - 1 : len - 2;
				memcpy (out+j, buf, octets);
				j += octets;
				memcpy (buf, buf + (len-1)*octets, octets);
				len = 1;
			}
		} else {
			if (len == 128 || end) {
				if (len > 1) {
					out[j++] = 257 - len;
					memcpy (out+j, buf, len*octets);
					j += len*octets;
				} else {
					out[j++] = 0;
					memcpy (out+j, buf, octets);
					j += octets;
				}
				len = 0;
				repeat = 0;
				if (end)
					continue;
			}
			if (!memcmp (buf + (len-1)*octets, buf + (len-2)*octets, octets)) {
				repeat = 1;
				if (len > 2) {
					out[j++] = 258 - len;
					memcpy (out+j, buf, (len-2)*octets);
					j += (len-2)*octets;
					memcpy (buf + octets, buf + (len-1)*octets, octets);
					memcpy (buf, buf + octets, octets);
					len = 2;
				}
				continue;
			}
		}
	}
	return j;
}

static size_t
_raster_write_page_header (FILE *out, struct raster_page_header *h)
{
	size_t c = 0;
	uint32_t opl;

	if (!out || !h)
		return 0;

	opl = _raster_octets_per_line (h->bits_per_pixel, h->width);

	c += _raster_write_string   (out, RASTER_PWGRASTER_STRING);
	c += _raster_write_string   (out, h->media_color);
	c += _raster_write_string   (out, h->media_type);
	c += _raster_write_string   (out, h->print_content_optimize);
	c += _raster_write_reserved (out, 12);
	c += _raster_write_uint     (out, h->cut_media);
	c += _raster_write_uint     (out, h->duplex);
	c += _raster_write_uint     (out, h->cross_resolution);
	c += _raster_write_uint     (out, h->feed_resolution);
	c += _raster_write_reserved (out, 16);
	c += _raster_write_uint     (out, h->insert_sheet);
	c += _raster_write_uint     (out, h->jog);
	c += _raster_write_uint     (out, h->leading_edge);
	c += _raster_write_reserved (out, 12);
	c += _raster_write_uint     (out, h->media_position);
	c += _raster_write_uint     (out, h->media_weight_metric);
	c += _raster_write_reserved (out, 8);
	c += _raster_write_uint     (out, h->num_copies);
	c += _raster_write_uint     (out, h->orientation);
	c += _raster_write_reserved (out, 4);
	c += _raster_write_uint     (out, h->cross_point_size);
	c += _raster_write_uint     (out, h->feed_point_size);
	c += _raster_write_reserved (out, 8);
	c += _raster_write_uint     (out, h->tumble);
	c += _raster_write_uint     (out, h->width);
	c += _raster_write_uint     (out, h->height);
	c += _raster_write_reserved (out, 4);
	c += _raster_write_uint     (out, h->bits_per_color);
	c += _raster_write_uint     (out, h->bits_per_pixel);
	c += _raster_write_uint     (out, opl);
	c += _raster_write_uint     (out, h->color_order);
	c += _raster_write_uint     (out, h->color_space);
	c += _raster_write_reserved (out, 16);
	c += _raster_write_uint     (out, h->num_colors);
	c += _raster_write_reserved (out, 28);
	c += _raster_write_uint     (out, h->total_page_count);
	c += _raster_write_int      (out, h->cross_feed_xform);
	c += _raster_write_int      (out, h->feed_xform);
	c += _raster_write_uint     (out, h->image_bbox_left);
	c += _raster_write_uint     (out, h->image_bbox_top);
	c += _raster_write_uint     (out, h->image_bbox_right);
	c += _raster_write_uint     (out, h->image_bbox_bottom);
	c += _raster_write_uint     (out, h->alternate_primary);
	c += _raster_write_uint     (out, h->print_quality);
	c += _raster_write_reserved (out, 20);
	c += _raster_write_uint     (out, h->vendor_identifier);
	c += _raster_write_uint     (out, h->vendor_data_length);
	c += _raster_write_vendor_data (out, h->vendor_data, h->vendor_data_length);
	c += _raster_write_reserved (out, 64);
	c += _raster_write_string   (out, h->rendering_intent);
	c += _raster_write_string   (out, h->page_size_name);

	return c;
}

/* packed_size is length which should write out to output, returns what does write out to output */
static size_t
_raster_write_pixmap (FILE *output, size_t *packed_size,
		uint8_t *pixbuf, size_t len,
		uint32_t width, uint32_t height, uint32_t bits_per_pixel)
{
	size_t cur_len, last_len, worst_case;
	size_t o, c, expected;
	uint8_t *cur_line, *last_line;
	uint32_t opp, line, rep;

	if (!output || !pixbuf || !width || !height || !bits_per_pixel || !len)
		return 0;

	o = c = expected = last_len = cur_len = 0;

	opp = _raster_octets_per_pixel (bits_per_pixel);
	worst_case = _raster_longest_packed_line (opp, width);

	last_line = calloc (worst_case, sizeof(uint8_t));
	cur_line  = calloc (worst_case, sizeof(uint8_t));

	if (!last_line || !cur_line)
		return 0;

	for (rep = line = 0; line < height; line++) {
		cur_len = _raster_pack_line (cur_line, pixbuf, width, opp);

		if (last_len == cur_len && rep < 255) {
			if (!memcmp (last_line, cur_line, cur_len)) {
				rep++;
				continue;
			}
		}

		expected += (1 + last_len);

		if (fputc (rep, output) == EOF)
			goto out;
		c++;

		o = fwrite (last_line, sizeof(uint8_t), last_len, output);
		c += o;
		if (o != cur_len)
			goto out;

		memcpy (last_line, cur_line, cur_len);
		last_len = cur_len;
		rep = 0;
	}

	/* now output the final line */
	expected += (1 + last_len);

	if (fputc (rep, output) == EOF)
		goto out;
	c++;

	o = fwrite (last_line, sizeof(uint8_t), last_len, output);
	c += o;

out:
	if (packed_size)
		*packed_size = expected;

	return c;
}

raster_t *raster_create (void)
{
	raster_t *raster;

	raster = calloc (1, sizeof(raster_t));

	if (raster)
		raster->header = calloc (1, sizeof(struct raster_page_header));
	else
		return raster;

	if (raster->header)
		raster->references = raster->is_ok = 1;

	return raster;
}

raster_t *raster_reference (raster_t *raster)
{
	if ( raster && raster->is_ok )
		raster->references++;

	return raster;
}

void raster_destroy (raster_t *raster)
{
	if ( raster && !(--raster->references) )
	{
		raster->is_ok = 0;
		if (raster->header)
			free (raster->header);
		if (raster->errorstring)
			free (raster->errorstring);
		free (raster);
	}
	return;
}

int
raster_set_output (raster_t* raster, FILE *output)
{
	if (!raster)
		return RASTER_NO_CONTEXT;

	if (!raster->header)
		return RASTER_NO_PAGE_HEADER;

	raster->output = output;

	return RASTER_OK;
}

int
raster_set_document_type (raster_t *raster, raster_document_type_t dt)
{
	if (!raster)
		return RASTER_NO_CONTEXT;

	if (!raster->header)
		return RASTER_NO_PAGE_HEADER;

	raster->header->bits_per_color = _raster_document_type[dt].bits_per_color;
	raster->header->bits_per_pixel = _raster_document_type[dt].bits_per_pixel;
	raster->header->color_space    = _raster_document_type[dt].color_space;
	raster->header->num_colors     = _raster_document_type[dt].num_colors;

	return RASTER_OK;
}

int
raster_set_media_color (raster_t* raster, const uint8_t *media_color, size_t len)
{
	size_t l;

	if (!raster)
		return RASTER_NO_CONTEXT;

	if (!raster->header)
		return RASTER_NO_PAGE_HEADER;

	l = _clamp (len, 0, RASTER_STRING_WIDTH);

	if (memcpy (raster->header->media_color, media_color, l))
		return RASTER_OK;
	else
		return RASTER_COPY_FAILED;
}

int
raster_set_vendor_id (raster_t* raster, uint32_t vendor_id)
{
	if (!raster)
		return RASTER_NO_CONTEXT;

	if (!raster->header)
		return RASTER_NO_PAGE_HEADER;

	raster->header->vendor_identifier = vendor_id;

	return RASTER_OK;
}

int
raster_set_vendor_data (raster_t* raster, const uint8_t *vendor_data, size_t len)
{
	size_t l;

	if (!raster)
		return RASTER_NO_CONTEXT;

	if (!raster->header)
		return RASTER_NO_PAGE_HEADER;

	l = _clamp (len, 0, RASTER_MAX_VENDOR_DATA);

	if (memcpy (raster->header->vendor_data, vendor_data, l))
		return RASTER_OK;
	else
		return RASTER_COPY_FAILED;
}

int
raster_set_media_type (raster_t* raster, const uint8_t *media_type, size_t len)
{
	size_t l;

	if (!raster)
		return RASTER_NO_CONTEXT;

	if (!raster->header)
		return RASTER_NO_PAGE_HEADER;

	l = _clamp (len, 0, RASTER_STRING_WIDTH);

	if (memcpy (raster->header->media_type, media_type, l))
		return RASTER_OK;
	else
		return RASTER_COPY_FAILED;
}

int
raster_set_page_size_name (raster_t* raster, const uint8_t *page_size_name, size_t len)
{
	size_t l;

	if (!raster)
		return RASTER_NO_CONTEXT;

	if (!raster->header)
		return RASTER_NO_PAGE_HEADER;

	l = _clamp (len, 0, RASTER_STRING_WIDTH);

	if (memcpy (raster->header->page_size_name, page_size_name, l))
		return RASTER_OK;
	else
		return RASTER_COPY_FAILED;
}

int
raster_set_print_content_optimize (raster_t* raster, const uint8_t *print_content_optimize, size_t len)
{
	size_t l;

	if (!raster)
		return RASTER_NO_CONTEXT;

	if (!raster->header)
		return RASTER_NO_PAGE_HEADER;

	l = _clamp (len, 0, RASTER_STRING_WIDTH);

	if (memcpy (raster->header->print_content_optimize, print_content_optimize, l))
		return RASTER_OK;
	else
		return RASTER_COPY_FAILED;
}

int
raster_set_rendering_intent (raster_t* raster, const uint8_t *rendering_intent, size_t len)
{
	size_t l;

	if (!raster)
		return RASTER_NO_CONTEXT;

	if (!raster->header)
		return RASTER_NO_PAGE_HEADER;

	l = _clamp (len, 0, RASTER_STRING_WIDTH);

	if (memcpy (raster->header->rendering_intent, rendering_intent, l))
		return RASTER_OK;
	else
		return RASTER_COPY_FAILED;
}

int
raster_set_color_order (raster_t* raster, raster_color_order_t color_order)
{
	if (!raster)
		return RASTER_NO_CONTEXT;

	if (!raster->header)
		return RASTER_NO_PAGE_HEADER;

	raster->header->color_order = color_order;

	return RASTER_OK;
}

int
raster_set_color_space (raster_t* raster, raster_color_space_t color_space)
{
	if (!raster)
		return RASTER_NO_CONTEXT;

	if (!raster->header)
		return RASTER_NO_PAGE_HEADER;

	raster->header->color_space = color_space;

	return RASTER_OK;
}

int
raster_set_leading_edge (raster_t* raster, raster_edge_t leading_edge)
{
	if (!raster)
		return RASTER_NO_CONTEXT;

	if (!raster->header)
		return RASTER_NO_PAGE_HEADER;

	raster->header->leading_edge = leading_edge;

	return RASTER_OK;
}

int
raster_set_media_position (raster_t* raster, raster_media_position_t media_position)
{
	if (!raster)
		return RASTER_NO_CONTEXT;

	if (!raster->header)
		return RASTER_NO_PAGE_HEADER;

	raster->header->media_position = media_position;

	return RASTER_OK;
}

int
raster_set_orientation (raster_t* raster, raster_orientation_t orientation)
{
	if (!raster)
		return RASTER_NO_CONTEXT;

	if (!raster->header)
		return RASTER_NO_PAGE_HEADER;

	raster->header->orientation = orientation;

	return RASTER_OK;
}

int
raster_set_print_quality (raster_t* raster, raster_print_quality_t print_quality)
{
	if (!raster)
		return RASTER_NO_CONTEXT;

	if (!raster->header)
		return RASTER_NO_PAGE_HEADER;

	raster->header->print_quality = print_quality;

	return RASTER_OK;
}

int
raster_set_cut_media (raster_t* raster, raster_when_t cut_media)
{
	if (!raster)
		return RASTER_NO_CONTEXT;

	if (!raster->header)
		return RASTER_NO_PAGE_HEADER;

	raster->header->cut_media = cut_media;

	return RASTER_OK;
}

int
raster_set_jog (raster_t* raster, raster_when_t jog)
{
	if (!raster)
		return RASTER_NO_CONTEXT;

	if (!raster->header)
		return RASTER_NO_PAGE_HEADER;

	raster->header->jog = jog;

	return RASTER_OK;
}

int
raster_set_xform (raster_t* raster, raster_xform_t cross_xform, raster_xform_t feed_xform)
{
	if (!raster)
		return RASTER_NO_CONTEXT;

	if (!raster->header)
		return RASTER_NO_PAGE_HEADER;

	raster->header->cross_feed_xform = cross_xform;
	raster->header->feed_xform = feed_xform;

	return RASTER_OK;
}

int
raster_set_alternate_primary (raster_t* raster, uint8_t alt_red, uint8_t alt_green, uint8_t alt_blue)
{
	if (!raster)
		return RASTER_NO_CONTEXT;

	if (!raster->header)
		return RASTER_NO_PAGE_HEADER;

	raster->header->alternate_primary = _raster_srgb32 (0, alt_red, alt_green, alt_blue);

	return RASTER_OK;
}

int
raster_set_bits_per_color (raster_t* raster, uint32_t bits_per_color)
{
	if (!raster)
		return RASTER_NO_CONTEXT;

	if (!raster->header)
		return RASTER_NO_PAGE_HEADER;

	raster->header->bits_per_color = bits_per_color;

	return RASTER_OK;
}

int
raster_set_bits_per_pixel (raster_t* raster, uint32_t bits_per_pixel)
{
	if (!raster)
		return RASTER_NO_CONTEXT;

	if (!raster->header)
		return RASTER_NO_PAGE_HEADER;

	raster->header-> bits_per_pixel= bits_per_pixel;

	return RASTER_OK;
}

int
raster_set_page_pixel_size (raster_t* raster, uint32_t cross_pixel_size, uint32_t feed_pixel_size)
{
	if (!raster)
		return RASTER_NO_CONTEXT;

	if (!raster->header)
		return RASTER_NO_PAGE_HEADER;

	raster->header->width = cross_pixel_size;
	raster->header->height  = feed_pixel_size;

	return RASTER_OK;
}

int
raster_set_resolution_dpi (raster_t* raster, uint32_t cross_resolution, uint32_t feed_resolution)
{
	if (!raster)
		return RASTER_NO_CONTEXT;

	if (!raster->header)
		return RASTER_NO_PAGE_HEADER;

	raster->header->cross_resolution = cross_resolution;
	raster->header->feed_resolution  = feed_resolution;

	return RASTER_OK;
}

int
raster_set_page_point_size (raster_t* raster, uint32_t cross_points, uint32_t feed_points)
{
	if (!raster)
		return RASTER_NO_CONTEXT;

	if (!raster->header)
		return RASTER_NO_PAGE_HEADER;

	raster->header->cross_point_size = cross_points;
	raster->header->feed_point_size  = feed_points;

	return RASTER_OK;
}

int
raster_set_bbox (raster_t* raster, uint32_t left, uint32_t right, uint32_t top, uint32_t bottom)
{
	if (!raster)
		return RASTER_NO_CONTEXT;

	if (!raster->header)
		return RASTER_NO_PAGE_HEADER;

	raster->header->image_bbox_left   = left;
	raster->header->image_bbox_right  = right;
	raster->header->image_bbox_top    = top;
	raster->header->image_bbox_bottom = bottom;

	return RASTER_OK;
}

int
raster_set_media_weight_metric (raster_t* raster, uint32_t media_weight_metric)
{
	if (!raster)
		return RASTER_NO_CONTEXT;

	if (!raster->header)
		return RASTER_NO_PAGE_HEADER;

	raster->header->media_weight_metric = media_weight_metric;

	return RASTER_OK;
}

int
raster_set_num_colors (raster_t* raster, uint32_t num_colors)
{
	if (!raster)
		return RASTER_NO_CONTEXT;

	if (!raster->header)
		return RASTER_NO_PAGE_HEADER;

	raster->header->num_colors = num_colors;

	return RASTER_OK;
}

int
raster_set_num_copies (raster_t* raster, uint32_t num_copies)
{
	if (!raster)
		return RASTER_NO_CONTEXT;

	if (!raster->header)
		return RASTER_NO_PAGE_HEADER;

	raster->header->num_copies = num_copies;

	return RASTER_OK;
}

int
raster_set_duplex (raster_t* raster, bool duplex)
{
	if (!raster)
		return RASTER_NO_CONTEXT;

	if (!raster->header)
		return RASTER_NO_PAGE_HEADER;

	raster->header->duplex = duplex ? 1 : 0;

	return RASTER_OK;
}

int
raster_set_insert_sheet (raster_t* raster, bool insert_sheet)
{
	if (!raster)
		return RASTER_NO_CONTEXT;

	if (!raster->header)
		return RASTER_NO_PAGE_HEADER;

	raster->header->insert_sheet = insert_sheet ? 1 : 0;

	return RASTER_OK;
}

int
raster_set_tumble (raster_t* raster, bool tumble)
{
	if (!raster)
		return RASTER_NO_CONTEXT;

	if (!raster->header)
		return RASTER_NO_PAGE_HEADER;

	raster->header->tumble = tumble ? 1 : 0;

	return RASTER_OK;
}

int
raster_set_sides_xform (raster_t* raster, raster_sides_t sides, raster_sheetback_t back)
{
	uint32_t d = 0, t = 0;
	int32_t x = RASTER_XFORM_NORMAL;
	int32_t f = RASTER_XFORM_NORMAL;

	if (!raster)
		return RASTER_NO_CONTEXT;

	if (!raster->header)
		return RASTER_NO_PAGE_HEADER;

	if (sides == RASTER_SIDES_ONE_SIDED)
		goto out;

	d = 1;

	if (sides == RASTER_SIDES_TWO_SIDED_LONG_EDGE) {

		if (back == RASTER_SHEETBACK_FLIPPED)
			f = RASTER_XFORM_REVERSED;

		if (back == RASTER_SHEETBACK_ROTATED)
			f = x = RASTER_XFORM_REVERSED;

	} else {

		t = 1;
		if (back == RASTER_SHEETBACK_FLIPPED)
			x = RASTER_XFORM_REVERSED;

		if (back == RASTER_SHEETBACK_MANUAL)
			f = x = RASTER_XFORM_REVERSED;

	}		

out:
	raster->header->duplex = d;
	raster->header->tumble = t;
	raster->header->feed_xform = f;
	raster->header->cross_feed_xform = x;

	return RASTER_OK;
}

int
raster_get_media_color (const raster_t *raster, uint8_t *media_color, size_t len)
{
	size_t l;

	if (!raster)
		return RASTER_NO_CONTEXT;

	if (!raster->header)
		return RASTER_NO_PAGE_HEADER;

	if (!media_color)
		return RASTER_BAD_ARG;

	l = _clamp (len, 0, RASTER_STRING_WIDTH);

	memcpy (media_color, raster->header->media_color, l);

	return RASTER_OK;
}

int
raster_get_vendor_id (const raster_t *raster, uint32_t *vendor_id)
{
	if (!raster)
		return RASTER_NO_CONTEXT;

	if (!raster->header)
		return RASTER_NO_PAGE_HEADER;

	if (!vendor_id)
		return RASTER_BAD_ARG;

	*vendor_id = raster->header->vendor_identifier;

	return RASTER_OK;
}

int
raster_get_vendor_data (const raster_t *raster, uint8_t *vendor_data, size_t len)
{
	size_t l;

	if (!raster)
		return RASTER_NO_CONTEXT;

	if (!raster->header)
		return RASTER_NO_PAGE_HEADER;

	if (!vendor_data)
		return RASTER_BAD_ARG;

	l = _clamp (len, 0, RASTER_MAX_VENDOR_DATA);

	memcpy (vendor_data, raster->header->vendor_data, l);

	return RASTER_OK;
}

int
raster_get_media_type (const raster_t *raster, uint8_t *media_type, size_t len)
{
	size_t l;

	if (!raster)
		return RASTER_NO_CONTEXT;

	if (!raster->header)
		return RASTER_NO_PAGE_HEADER;

	if (!media_type)
		return RASTER_BAD_ARG;

	l = _clamp (len, 0, RASTER_STRING_WIDTH);

	memcpy (media_type, raster->header->media_type, l);

	return RASTER_OK;
}

int
raster_get_page_size_name (const raster_t *raster, uint8_t *page_size_name, size_t len)
{
	size_t l;

	if (!raster)
		return RASTER_NO_CONTEXT;

	if (!raster->header)
		return RASTER_NO_PAGE_HEADER;

	if (!page_size_name)
		return RASTER_BAD_ARG;

	l = _clamp (len, 0, RASTER_STRING_WIDTH);

	memcpy (page_size_name, raster->header->page_size_name, l);

	return RASTER_OK;
}

int
raster_get_print_content_optimize (const raster_t *raster, uint8_t *print_content_optimize, size_t len)
{
	size_t l;

	if (!raster)
		return RASTER_NO_CONTEXT;

	if (!raster->header)
		return RASTER_NO_PAGE_HEADER;

	if (!print_content_optimize)
		return RASTER_BAD_ARG;

	l = _clamp (len, 0, RASTER_STRING_WIDTH);

	memcpy (print_content_optimize, raster->header->print_content_optimize, l);

	return RASTER_OK;
}

int
raster_get_rendering_intent (const raster_t *raster, uint8_t *rendering_intent, size_t len)
{
	size_t l;

	if (!raster)
		return RASTER_NO_CONTEXT;

	if (!raster->header)
		return RASTER_NO_PAGE_HEADER;

	if (!rendering_intent)
		return RASTER_BAD_ARG;

	l = _clamp (len, 0, RASTER_STRING_WIDTH);

	memcpy (rendering_intent, raster->header->rendering_intent, l);

	return RASTER_OK;
}

int
raster_get_color_order (const raster_t *raster, raster_color_order_t *color_order)
{
	if (!raster)
		return RASTER_NO_CONTEXT;

	if (!raster->header)
		return RASTER_NO_PAGE_HEADER;

	if (!color_order)
		return RASTER_BAD_ARG;

	*color_order = raster->header->color_order;

	return RASTER_OK;
}

int
raster_get_color_space (const raster_t *raster, raster_color_space_t *color_space)
{
	if (!raster)
		return RASTER_NO_CONTEXT;

	if (!raster->header)
		return RASTER_NO_PAGE_HEADER;

	if (!color_space)
		return RASTER_BAD_ARG;

	*color_space = raster->header->color_space;

	return RASTER_OK;
}

int
raster_get_leading_edge (const raster_t *raster, raster_edge_t *leading_edge)
{
	if (!raster)
		return RASTER_NO_CONTEXT;

	if (!raster->header)
		return RASTER_NO_PAGE_HEADER;

	if (!leading_edge)
		return RASTER_BAD_ARG;

	*leading_edge = raster->header->leading_edge;

	return RASTER_OK;
}

int
raster_get_media_position (const raster_t *raster, raster_media_position_t *media_position)
{
	if (!raster)
		return RASTER_NO_CONTEXT;

	if (!raster->header)
		return RASTER_NO_PAGE_HEADER;

	if (!media_position)
		return RASTER_BAD_ARG;

	*media_position = raster->header->media_position;

	return RASTER_OK;
}

int
raster_get_orientation (const raster_t *raster, raster_orientation_t *orientation)
{
	if (!raster)
		return RASTER_NO_CONTEXT;

	if (!raster->header)
		return RASTER_NO_PAGE_HEADER;

	if (!orientation)
		return RASTER_BAD_ARG;

	*orientation = raster->header->orientation;

	return RASTER_OK;
}

int
raster_get_print_quality (const raster_t *raster, raster_print_quality_t *print_quality)
{
	if (!raster)
		return RASTER_NO_CONTEXT;

	if (!raster->header)
		return RASTER_NO_PAGE_HEADER;

	if (!print_quality)
		return RASTER_BAD_ARG;

	*print_quality = raster->header->print_quality;

	return RASTER_OK;
}

int
raster_get_cut_media (const raster_t *raster, raster_when_t *cut_media)
{
	if (!raster)
		return RASTER_NO_CONTEXT;

	if (!raster->header)
		return RASTER_NO_PAGE_HEADER;

	if (!cut_media)
		return RASTER_BAD_ARG;

	*cut_media = raster->header->cut_media;

	return RASTER_OK;
}

int
raster_get_jog (const raster_t *raster, raster_when_t *jog)
{
	if (!raster)
		return RASTER_NO_CONTEXT;

	if (!raster->header)
		return RASTER_NO_PAGE_HEADER;

	if (!jog)
		return RASTER_BAD_ARG;

	*jog = raster->header->jog;

	return RASTER_OK;
}

int
raster_get_cross_feed_xform (const raster_t *raster, raster_xform_t *cross_xform, raster_xform_t *feed_xform)
{
	if (!raster)
		return RASTER_NO_CONTEXT;

	if (!raster->header)
		return RASTER_NO_PAGE_HEADER;

	if (!cross_xform || !feed_xform)
		return RASTER_BAD_ARG;

	*cross_xform = raster->header->cross_feed_xform;
	*feed_xform  = raster->header->feed_xform;

	return RASTER_OK;
}

int
raster_get_alternate_primary (const raster_t *raster, uint8_t *red, uint8_t *green, uint8_t *blue)
{
	uint8_t x;

	if (!raster)
		return RASTER_NO_CONTEXT;

	if (!raster->header)
		return RASTER_NO_PAGE_HEADER;

	if (!red || !green || !blue)
		return RASTER_BAD_ARG;

	_raster_split_srgb32 (&x, red, green, blue, raster->header->alternate_primary);

	return RASTER_OK;
}

int
raster_get_bits_per_color (const raster_t *raster, uint32_t *bits_per_color)
{
	if (!raster)
		return RASTER_NO_CONTEXT;

	if (!raster->header)
		return RASTER_NO_PAGE_HEADER;

	if (!bits_per_color)
		return RASTER_BAD_ARG;

	*bits_per_color = raster->header->bits_per_color;

	return RASTER_OK;
}

int
raster_get_bits_per_pixel (const raster_t *raster, uint32_t *bits_per_pixel)
{
	if (!raster)
		return RASTER_NO_CONTEXT;

	if (!raster->header)
		return RASTER_NO_PAGE_HEADER;

	if (!bits_per_pixel)
		return RASTER_BAD_ARG;

	*bits_per_pixel = raster->header->bits_per_pixel;

	return RASTER_OK;
}

int
raster_get_page_pixel_size (const raster_t *raster, uint32_t *cross_pixel_size, uint32_t *feed_pixel_size)
{
	if (!raster)
		return RASTER_NO_CONTEXT;

	if (!raster->header)
		return RASTER_NO_PAGE_HEADER;

	if (!cross_pixel_size || !feed_pixel_size)
		return RASTER_BAD_ARG;

	*cross_pixel_size = raster->header->width;
	*feed_pixel_size  = raster->header->height;

	return RASTER_OK;
}

int
raster_get_resolution_dpi (const raster_t *raster, uint32_t *cross_dpi, uint32_t *feed_dpi)
{
	if (!raster)
		return RASTER_NO_CONTEXT;

	if (!raster->header)
		return RASTER_NO_PAGE_HEADER;

	if (!cross_dpi || !feed_dpi)
		return RASTER_BAD_ARG;

	*cross_dpi = raster->header->cross_resolution;
	*feed_dpi  = raster->header->feed_resolution;

	return RASTER_OK;
}

int
raster_get_page_point_size (const raster_t *raster, uint32_t *cross_points, uint32_t *feed_points)
{
	if (!raster)
		return RASTER_NO_CONTEXT;

	if (!raster->header)
		return RASTER_NO_PAGE_HEADER;

	if (!cross_points || !feed_points)
		return RASTER_BAD_ARG;

	*cross_points = raster->header->cross_point_size;
	*feed_points  = raster->header->feed_point_size;

	return RASTER_OK;
}

int
raster_get_bbox (const raster_t *raster, uint32_t *left, uint32_t *right, uint32_t *top, uint32_t *bottom)
{
	if (!raster)
		return RASTER_NO_CONTEXT;

	if (!raster->header)
		return RASTER_NO_PAGE_HEADER;

	if (!left || !right || !top || !bottom)
		return RASTER_BAD_ARG;

	*left   = raster->header->image_bbox_left;
	*right  = raster->header->image_bbox_right;
	*top    = raster->header->image_bbox_top;
	*bottom = raster->header->image_bbox_bottom;

	return RASTER_OK;
}

int
raster_get_media_weight_metric (const raster_t *raster, uint32_t *media_weight_metric)
{
	if (!raster)
		return RASTER_NO_CONTEXT;

	if (!raster->header)
		return RASTER_NO_PAGE_HEADER;

	if (!media_weight_metric)
		return RASTER_BAD_ARG;

	*media_weight_metric = raster->header->media_weight_metric;

	return RASTER_OK;
}

int
raster_get_num_colors (const raster_t *raster, uint32_t *num_colors)
{
	if (!raster)
		return RASTER_NO_CONTEXT;

	if (!raster->header)
		return RASTER_NO_PAGE_HEADER;

	if (!num_colors)
		return RASTER_BAD_ARG;

	*num_colors = raster->header->num_colors;

	return RASTER_OK;
}

int
raster_get_num_copies (const raster_t *raster, uint32_t *num_copies)
{
	if (!raster)
		return RASTER_NO_CONTEXT;

	if (!raster->header)
		return RASTER_NO_PAGE_HEADER;

	if (!num_copies)
		return RASTER_BAD_ARG;

	*num_copies = raster->header->num_copies;

	return RASTER_OK;
}

int
raster_get_duplex (const raster_t *raster, bool *duplex)
{
	if (!raster)
		return RASTER_NO_CONTEXT;

	if (!raster->header)
		return RASTER_NO_PAGE_HEADER;

	if (!duplex)
		return RASTER_BAD_ARG;

	*duplex = raster->header->duplex ? true : false;

	return RASTER_OK;
}

int
raster_get_insert_sheet (const raster_t *raster, bool *insert_sheet)
{
	if (!raster)
		return RASTER_NO_CONTEXT;

	if (!raster->header)
		return RASTER_NO_PAGE_HEADER;

	if (!insert_sheet)
		return RASTER_BAD_ARG;

	*insert_sheet = raster->header->insert_sheet ? true : false;

	return RASTER_OK;
}

int
raster_get_tumble (const raster_t *raster, bool *tumble)
{
	if (!raster)
		return RASTER_NO_CONTEXT;

	if (!raster->header)
		return RASTER_NO_PAGE_HEADER;

	if (!tumble)
		return RASTER_BAD_ARG;

	*tumble = raster->header->tumble ? true : false;

	return RASTER_OK;
}

FILE *
raster_get_output (const raster_t *raster)
{
	if (!raster)
		return NULL;

	return raster->output;
}

char *
raster_get_error_string (const raster_t *raster)
{
	if (!raster)
		return NULL;

	return raster->errorstring;
}

size_t
raster_get_octets_written (const raster_t *raster)
{
	if (!raster)
		return 0;

	return raster->octets_out;
}

int
raster_clear_octets_written (raster_t *raster)
{
	if (!raster)
		return RASTER_NO_CONTEXT;

	raster->octets_out = 0;

	return RASTER_OK;
}

int
raster_clear_error_string (raster_t *raster)
{
	if (!raster)
		return RASTER_NO_CONTEXT;

	if (raster->errorstring)
		free (raster->errorstring);

	return RASTER_OK;
}

int
raster_write_magic (raster_t *raster)
{
	static const uint8_t RaS2[RASTER_MAGIC_SIZE] = { 0x52, 0x61, 0x53, 0x32 };
	size_t o;

	if (!raster)
		return RASTER_NO_CONTEXT;

	if (!raster->output)
		return RASTER_NO_OUTPUT;

	o = fwrite (RaS2, sizeof(uint8_t), RASTER_MAGIC_SIZE, raster->output);

	raster->octets_out += o;

	if (o != RASTER_MAGIC_SIZE)
		return RASTER_SHORT_WRITE;

	return RASTER_OK;
}

int
raster_write_page_header (raster_t *raster)
{
	size_t o;

	if (!raster)
		return RASTER_NO_CONTEXT;

	if (!raster->output)
		return RASTER_NO_OUTPUT;

	if (!raster->header)
		return RASTER_NO_PAGE_HEADER;

	o = _raster_write_page_header (raster->output, raster->header);

	raster->octets_out += o;

	if (o != RASTER_PAGE_HEADER_SIZE)
		return RASTER_SHORT_WRITE;

	return RASTER_OK;
}

int
raster_write_pixmap (raster_t *raster, uint8_t *pixbuf, size_t len)
{
	size_t o, packed;

	if (!raster)
		return RASTER_NO_CONTEXT;

	if (!raster->output)
		return RASTER_NO_OUTPUT;

	if (!pixbuf || !len)
		return RASTER_NO_PIXBUF;

	o = _raster_write_pixmap (raster->output, &packed, pixbuf, len,
				raster->header->width, raster->header->height,
				raster->header->bits_per_pixel);

	raster->octets_out += o;

	if (o != packed)
		return RASTER_SHORT_WRITE;

	return RASTER_OK;
}

int
raster_write_page (raster_t *raster, uint8_t *pixbuf, size_t len)
{
	size_t o, packed;

	if (!raster)
		return RASTER_NO_CONTEXT;

	if (!raster->output)
		return RASTER_NO_OUTPUT;

	if (!raster->header)
		return RASTER_NO_PAGE_HEADER;

	if (!pixbuf || !len)
		return RASTER_NO_PIXBUF;

	o = _raster_write_page_header (raster->output, raster->header);

	raster->octets_out += o;

	if (o != RASTER_PAGE_HEADER_SIZE)
		return RASTER_SHORT_WRITE;

	o = _raster_write_pixmap (raster->output, &packed, pixbuf, len,
				raster->header->width, raster->header->height,
				raster->header->bits_per_pixel);

	raster->octets_out += o;

	if (o != packed)
		return RASTER_SHORT_WRITE;

	return RASTER_OK;
}

raster_color_space_t
raster_color_space_from_keyword (uint8_t *kw)
{
	int i, j;
	size_t l;
	const struct _raster_keyword *sk;

	l = strlen (kw);

	for (i = 0; _raster_color_space[i].keywords != NULL; i++)
		for (j = 0; _raster_color_space[i].keywords[j].kw != NULL; j++)	{
			sk = &_raster_color_space[i].keywords[j];
			if (l >= sk->min && !strncasecmp (sk->kw, kw, l))
				return _raster_color_space[i].color_space;
		}

	return RASTER_COLOR_SPACE_UNKNOWN;
};

raster_edge_t
raster_edge_from_keyword (uint8_t *kw)
{
	int i, j;
	size_t l;
	const struct _raster_keyword *sk;

	l = strlen (kw);

	for (i = 0; _raster_edge[i].keywords != NULL; i++)
		for (j = 0; _raster_edge[i].keywords[j].kw != NULL; j++) {
			sk = &_raster_edge[i].keywords[j];
			if (l >= sk->min && !strncasecmp (sk->kw, kw, l))
				return _raster_edge[i].edge;
		}

	return RASTER_EDGE_SHORT;
};

raster_media_position_t
raster_media_position_from_keyword (uint8_t *kw)
{
	int i, j;
	size_t l;
	const struct _raster_keyword *sk;

	l = strlen (kw);

	for (i = 0; _raster_media_position[i].keywords != NULL; i++)
		for (j = 0; _raster_media_position[i].keywords[j].kw != NULL; j++) {
			sk = &_raster_media_position[i].keywords[j];
			if (l >= sk->min && !strncasecmp (sk->kw, kw, l))
				return _raster_media_position[i].media_position;
		}

	return RASTER_MEDIA_POSITION_AUTO;
};

raster_orientation_t
raster_orientation_from_keyword (uint8_t *kw)
{
	int i, j;
	size_t l;
	const struct _raster_keyword *sk;

	l = strlen (kw);

	for (i = 0; _raster_orientation[i].keywords != NULL; i++)
		for (j = 0; _raster_orientation[i].keywords[j].kw != NULL; j++) {
			sk = &_raster_orientation[i].keywords[j];
			if (l >= sk->min && !strncasecmp (sk->kw, kw, l))
				return _raster_orientation[i].orientation;
		}

	return RASTER_ORIENTATION_PORTRAIT;
};

raster_when_t
raster_when_from_keyword (uint8_t *kw)
{
	int i, j;
	size_t l;
	const struct _raster_keyword *sk;

	l = strlen (kw);

	for (i = 0; _raster_when[i].keywords != NULL; i++) {
		for (j = 0; _raster_when[i].keywords[j].kw != NULL; j++)
			sk = &_raster_when[i].keywords[j];
		if (l >= sk->min && !strncasecmp (sk->kw, kw, l))
			return _raster_when[i].when;
	}

	return RASTER_WHEN_NEVER;
};

raster_sides_t
raster_sides_from_keyword (uint8_t *kw)
{
	int i, j;
	size_t l;
	const struct _raster_keyword *sk;

	l = strlen (kw);

	for (i = 0; _raster_sides[i].keywords != NULL; i++)
		for (j = 0; _raster_sides[i].keywords[j].kw != NULL; j++) {
			sk = &_raster_sides[i].keywords[j];
			if (l >= sk->min && !strncasecmp (sk->kw, kw, l))
				return _raster_sides[i].sides;
		}

	return RASTER_SIDES_ONE_SIDED;
};

raster_sheetback_t
raster_sheetback_from_keyword (uint8_t *kw)
{
	int i, j;
	size_t l;
	const struct _raster_keyword *sk;

	l = strlen (kw);

	for (i = 0; _raster_sheetback[i].keywords != NULL; i++)
		for (j = 0; _raster_sheetback[i].keywords[j].kw != NULL; j++) {
			sk = &_raster_sheetback[i].keywords[j];
			if (l >= sk->min && !strncasecmp (sk->kw, kw, l))
				return _raster_sheetback[i].sheetback;
		}

	return RASTER_SHEETBACK_NORMAL;
};

raster_print_quality_t
raster_print_quality_from_keyword (uint8_t *kw)
{
	int i, j;
	size_t l;
	const struct _raster_keyword *sk;

	l = strlen (kw);

	for (i = 0; _raster_print_quality[i].keywords != NULL; i++)
		for (j = 0; _raster_print_quality[i].keywords[j].kw != NULL; j++) {
			sk = &_raster_print_quality[i].keywords[j];
			if (l >= sk->min && !strncasecmp (sk->kw, kw, l))
				return _raster_print_quality[i].print_quality;
		}

	return RASTER_QUALITY_DEFAULT;
};

raster_xform_t
raster_xform_from_keyword (uint8_t *kw)
{
	int i, j;
	size_t l;
	const struct _raster_keyword *sk;

	l = strlen (kw);

	for (i = 0; _raster_xform[i].keywords != NULL; i++)
		for (j = 0; _raster_xform[i].keywords[j].kw != NULL; j++) {
			sk = &_raster_xform[i].keywords[j];
			if (l >= sk->min && !strncasecmp (sk->kw, kw, l))
				return _raster_xform[i].xform;
		}

	return RASTER_XFORM_UNKNOWN;
};

raster_document_type_t
raster_document_type_from_keyword (uint8_t *kw)
{
	int i, j;
	size_t l;
	const struct _raster_keyword *sk;

	l = strlen (kw);

	for (i = 0; _raster_document_type[i].keywords != NULL; i++)
		for (j = 0; _raster_document_type[i].keywords[j].kw != NULL; j++) {
			sk = &_raster_document_type[i].keywords[j];
			if (l >= sk->min && !strncasecmp (sk->kw, kw, l))
				return _raster_document_type[i].document_type;
		}

	return RASTER_DOCUMENT_TYPE_UNKNOWN;
};

bool
raster_bool_from_keyword (uint8_t *kw)
{
	if (kw) {
		switch (kw[0]) {
		case 't':
		case 'y':
		case 'T':
		case 'Y':
		case '1':
			return true;
		default:
			return false;
		}
	}

	return false;
}
