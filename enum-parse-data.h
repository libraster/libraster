/*
 * Copyright 2012-2013 James Cloos
 *
 *
 * This file is part of LibRaster.
 *
 * LibRaster is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * LibRaster is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with LibRaster.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "raster-private.h"

struct _raster_keyword {
	const char  *kw;
	const size_t min;
};

struct _raster_color_order {
	const struct _raster_keyword *keywords;
	const raster_color_order_t color_order;
};

static const struct _raster_keyword _raster_color_order_chunked_keywords[] =
{
	{
		.kw = "chunked",
		.min = 1
	},
	{
		.kw = "chunk",
		.min = 1
	},
	{
		.kw = NULL,
		.min = 0
	}
};

static const struct _raster_color_order _raster_color_order[] =
{
	{
		.color_order = RASTER_COLOR_ORDER_CHUNKED,
		.keywords = _raster_color_order_chunked_keywords
	},

	{
		.color_order = 1,
		.keywords = NULL
	}
};

struct _raster_color_space {
	const struct _raster_keyword *keywords;
	const raster_color_space_t color_space;
};

static const struct _raster_keyword _raster_color_space_rgb_keywords[] =
{
	{
		.kw = "rgb",
		.min = 1
	},
	{
		.kw = NULL,
		.min = 0
	}
};

static const struct _raster_keyword _raster_color_space_k_keywords[] =
{
	{
		.kw = "k",
		.min = 1
	},
	{
		.kw = "black",
		.min = 1
	},
	{
		.kw = NULL,
		.min = 0
	}
};

static const struct _raster_keyword _raster_color_space_cmyk_keywords[] =
{
	{
		.kw = "cmyk",
		.min = 1
	},
	{
		.kw = NULL,
		.min = 0
	}
};

static const struct _raster_keyword _raster_color_space_sw_keywords[] =
{
	{
		.kw = "sw",
		.min = 2
	},
	{
		.kw = "sgray",
		.min = 2
	},
	{
		.kw = NULL,
		.min = 0
	}
};

static const struct _raster_keyword _raster_color_space_srgb_keywords[] =
{
	{
		.kw = "srgb",
		.min = 2
	},
	{
		.kw = NULL,
		.min = 0
	}
};

static const struct _raster_keyword _raster_color_space_adobergb_keywords[] =
{
	{
		.kw = "adobergb",
		.min = 1
	},
	{
		.kw = "adobe-rgb",
		.min = 1
	},
	{
		.kw = "adobe_rgb",
		.min = 1
	},
	{
		.kw = NULL,
		.min = 0
	}
};

static const struct _raster_keyword _raster_color_space_device_1_keywords[] =
{
	{
		.kw = "device_1",
		.min = 8
	},
	{
		.kw = "device-1",
		.min = 8
	},
	{
		.kw = "device1",
		.min = 7
	},
	{
		.kw = "dev_1",
		.min = 5
	},
	{
		.kw = "dev-1",
		.min = 5
	},
	{
		.kw = "dev1",
		.min = 5
	},
	{
		.kw = NULL,
		.min = 0
	}
};

static const struct _raster_keyword _raster_color_space_device_2_keywords[] =
{
	{
		.kw = "device_2",
		.min = 8
	},
	{
		.kw = "device-2",
		.min = 8
	},
	{
		.kw = "device2",
		.min = 7
	},
	{
		.kw = "dev_2",
		.min = 5
	},
	{
		.kw = "dev-2",
		.min = 5
	},
	{
		.kw = "dev2",
		.min = 5
	},
	{
		.kw = NULL,
		.min = 0
	}
};

static const struct _raster_keyword _raster_color_space_device_3_keywords[] =
{
	{
		.kw = "device_3",
		.min = 8
	},
	{
		.kw = "device-3",
		.min = 8
	},
	{
		.kw = "device3",
		.min = 7
	},
	{
		.kw = "dev_3",
		.min = 5
	},
	{
		.kw = "dev-3",
		.min = 5
	},
	{
		.kw = "dev3",
		.min = 5
	},
	{
		.kw = NULL,
		.min = 0
	}
};

static const struct _raster_keyword _raster_color_space_device_4_keywords[] =
{
	{
		.kw = "device_4",
		.min = 8
	},
	{
		.kw = "device-4",
		.min = 8
	},
	{
		.kw = "device4",
		.min = 7
	},
	{
		.kw = "dev_4",
		.min = 5
	},
	{
		.kw = "dev-4",
		.min = 5
	},
	{
		.kw = "dev4",
		.min = 5
	},
	{
		.kw = NULL,
		.min = 0
	}
};

static const struct _raster_keyword _raster_color_space_device_5_keywords[] =
{
	{
		.kw = "device_5",
		.min = 8
	},
	{
		.kw = "device-5",
		.min = 8
	},
	{
		.kw = "device5",
		.min = 7
	},
	{
		.kw = "dev_5",
		.min = 5
	},
	{
		.kw = "dev-5",
		.min = 5
	},
	{
		.kw = "dev5",
		.min = 5
	},
	{
		.kw = NULL,
		.min = 0
	}
};

static const struct _raster_keyword _raster_color_space_device_6_keywords[] =
{
	{
		.kw = "device_6",
		.min = 8
	},
	{
		.kw = "device-6",
		.min = 8
	},
	{
		.kw = "device6",
		.min = 7
	},
	{
		.kw = "dev_6",
		.min = 5
	},
	{
		.kw = "dev-6",
		.min = 5
	},
	{
		.kw = "dev6",
		.min = 5
	},
	{
		.kw = NULL,
		.min = 0
	}
};

static const struct _raster_keyword _raster_color_space_device_7_keywords[] =
{
	{
		.kw = "device_7",
		.min = 8
	},
	{
		.kw = "device-7",
		.min = 8
	},
	{
		.kw = "device7",
		.min = 7
	},
	{
		.kw = "dev_7",
		.min = 5
	},
	{
		.kw = "dev-7",
		.min = 5
	},
	{
		.kw = "dev7",
		.min = 5
	},
	{
		.kw = NULL,
		.min = 0
	}
};

static const struct _raster_keyword _raster_color_space_device_8_keywords[] =
{
	{
		.kw = "device_8",
		.min = 8
	},
	{
		.kw = "device-8",
		.min = 8
	},
	{
		.kw = "device8",
		.min = 7
	},
	{
		.kw = "dev_8",
		.min = 5
	},
	{
		.kw = "dev-8",
		.min = 5
	},
	{
		.kw = "dev8",
		.min = 5
	},
	{
		.kw = NULL,
		.min = 0
	}
};

static const struct _raster_keyword _raster_color_space_device_9_keywords[] =
{
	{
		.kw = "device_9",
		.min = 8
	},
	{
		.kw = "device-9",
		.min = 8
	},
	{
		.kw = "device9",
		.min = 7
	},
	{
		.kw = "dev_9",
		.min = 5
	},
	{
		.kw = "dev-9",
		.min = 5
	},
	{
		.kw = "dev9",
		.min = 5
	},
	{
		.kw = NULL,
		.min = 0
	}
};

static const struct _raster_keyword _raster_color_space_device_10_keywords[] =
{
	{
		.kw = "device_10",
		.min = 9
	},
	{
		.kw = "device-10",
		.min = 9
	},
	{
		.kw = "device10",
		.min = 8
	},
	{
		.kw = "dev_10",
		.min = 6
	},
	{
		.kw = "dev-10",
		.min = 6
	},
	{
		.kw = "dev10",
		.min = 5
	},
	{
		.kw = "device_a",
		.min = 8
	},
	{
		.kw = "device-a",
		.min = 8
	},
	{
		.kw = "devicea",
		.min = 7
	},
	{
		.kw = "dev_a",
		.min = 5
	},
	{
		.kw = "dev-a",
		.min = 5
	},
	{
		.kw = "deva",
		.min = 4
	},
	{
		.kw = NULL,
		.min = 0
	}
};

static const struct _raster_keyword _raster_color_space_device_11_keywords[] =
{
	{
		.kw = "device_11",
		.min = 9
	},
	{
		.kw = "device-11",
		.min = 9
	},
	{
		.kw = "device11",
		.min = 8
	},
	{
		.kw = "dev_11",
		.min = 6
	},
	{
		.kw = "dev-11",
		.min = 6
	},
	{
		.kw = "dev11",
		.min = 5
	},
	{
		.kw = "device_b",
		.min = 8
	},
	{
		.kw = "device-b",
		.min = 8
	},
	{
		.kw = "deviceb",
		.min = 7
	},
	{
		.kw = "dev_b",
		.min = 5
	},
	{
		.kw = "dev-b",
		.min = 5
	},
	{
		.kw = "devb",
		.min = 4
	},
	{
		.kw = NULL,
		.min = 0
	}
};

static const struct _raster_keyword _raster_color_space_device_12_keywords[] =
{
	{
		.kw = "device_12",
		.min = 9
	},
	{
		.kw = "device-12",
		.min = 9
	},
	{
		.kw = "device12",
		.min = 8
	},
	{
		.kw = "dev_12",
		.min = 6
	},
	{
		.kw = "dev-12",
		.min = 6
	},
	{
		.kw = "dev12",
		.min = 5
	},
	{
		.kw = "device_c",
		.min = 8
	},
	{
		.kw = "device-c",
		.min = 8
	},
	{
		.kw = "devicec",
		.min = 7
	},
	{
		.kw = "dev_c",
		.min = 5
	},
	{
		.kw = "dev-c",
		.min = 5
	},
	{
		.kw = "devc",
		.min = 4
	},
	{
		.kw = NULL,
		.min = 0
	}
};

static const struct _raster_keyword _raster_color_space_device_13_keywords[] =
{
	{
		.kw = "device_13",
		.min = 9
	},
	{
		.kw = "device-13",
		.min = 9
	},
	{
		.kw = "device13",
		.min = 8
	},
	{
		.kw = "dev_13",
		.min = 6
	},
	{
		.kw = "dev-13",
		.min = 6
	},
	{
		.kw = "dev13",
		.min = 5
	},
	{
		.kw = "device_d",
		.min = 8
	},
	{
		.kw = "device-d",
		.min = 8
	},
	{
		.kw = "deviced",
		.min = 7
	},
	{
		.kw = "dev_d",
		.min = 5
	},
	{
		.kw = "dev-d",
		.min = 5
	},
	{
		.kw = "devd",
		.min = 4
	},
	{
		.kw = NULL,
		.min = 0
	}
};

static const struct _raster_keyword _raster_color_space_device_14_keywords[] =
{
	{
		.kw = "device_14",
		.min = 9
	},
	{
		.kw = "device-14",
		.min = 9
	},
	{
		.kw = "device14",
		.min = 8
	},
	{
		.kw = "dev_14",
		.min = 6
	},
	{
		.kw = "dev-14",
		.min = 6
	},
	{
		.kw = "dev14",
		.min = 5
	},
	{
		.kw = "device_e",
		.min = 8
	},
	{
		.kw = "device-e",
		.min = 8
	},
	{
		.kw = "devicee",
		.min = 7
	},
	{
		.kw = "dev_e",
		.min = 5
	},
	{
		.kw = "dev-e",
		.min = 5
	},
	{
		.kw = "deve",
		.min = 4
	},
	{
		.kw = NULL,
		.min = 0
	}
};

static const struct _raster_keyword _raster_color_space_device_15_keywords[] =
{
	{
		.kw = "device_15",
		.min = 9
	},
	{
		.kw = "device-15",
		.min = 9
	},
	{
		.kw = "device15",
		.min = 8
	},
	{
		.kw = "dev_15",
		.min = 6
	},
	{
		.kw = "dev-15",
		.min = 6
	},
	{
		.kw = "dev15",
		.min = 5
	},
	{
		.kw = "device_f",
		.min = 8
	},
	{
		.kw = "device-f",
		.min = 8
	},
	{
		.kw = "devicef",
		.min = 7
	},
	{
		.kw = "dev_f",
		.min = 5
	},
	{
		.kw = "dev-f",
		.min = 5
	},
	{
		.kw = "devf",
		.min = 4
	},
	{
		.kw = NULL,
		.min = 0
	}
};

static const struct _raster_color_space _raster_color_space[] =
{
	{
		.color_space = RASTER_COLOR_SPACE_RGB,
		.keywords = _raster_color_space_rgb_keywords
	},

	{
		.color_space = RASTER_COLOR_SPACE_K,
		.keywords = _raster_color_space_k_keywords
	},

	{
		.color_space = RASTER_COLOR_SPACE_CMYK,
		.keywords = _raster_color_space_cmyk_keywords
	},

	{
		.color_space = RASTER_COLOR_SPACE_SW,
		.keywords = _raster_color_space_sw_keywords
	},

	{
		.color_space = RASTER_COLOR_SPACE_SRGB,
		.keywords = _raster_color_space_srgb_keywords
	},

	{
		.color_space = RASTER_COLOR_SPACE_ADOBERGB,
		.keywords = _raster_color_space_adobergb_keywords
	},

	{
		.color_space = RASTER_COLOR_SPACE_DEVICE_1,
		.keywords = _raster_color_space_device_1_keywords
	},

	{
		.color_space = RASTER_COLOR_SPACE_DEVICE_2,
		.keywords = _raster_color_space_device_2_keywords
	},

	{
		.color_space = RASTER_COLOR_SPACE_DEVICE_3,
		.keywords = _raster_color_space_device_3_keywords
	},

	{
		.color_space = RASTER_COLOR_SPACE_DEVICE_4,
		.keywords = _raster_color_space_device_4_keywords
	},

	{
		.color_space = RASTER_COLOR_SPACE_DEVICE_5,
		.keywords = _raster_color_space_device_5_keywords
	},

	{
		.color_space = RASTER_COLOR_SPACE_DEVICE_6,
		.keywords = _raster_color_space_device_6_keywords
	},

	{
		.color_space = RASTER_COLOR_SPACE_DEVICE_7,
		.keywords = _raster_color_space_device_7_keywords
	},

	{
		.color_space = RASTER_COLOR_SPACE_DEVICE_8,
		.keywords = _raster_color_space_device_8_keywords
	},

	{
		.color_space = RASTER_COLOR_SPACE_DEVICE_9,
		.keywords = _raster_color_space_device_9_keywords
	},

	{
		.color_space = RASTER_COLOR_SPACE_DEVICE_10,
		.keywords = _raster_color_space_device_10_keywords
	},

	{
		.color_space = RASTER_COLOR_SPACE_DEVICE_11,
		.keywords = _raster_color_space_device_11_keywords
	},

	{
		.color_space = RASTER_COLOR_SPACE_DEVICE_12,
		.keywords = _raster_color_space_device_12_keywords
	},

	{
		.color_space = RASTER_COLOR_SPACE_DEVICE_13,
		.keywords = _raster_color_space_device_13_keywords
	},

	{
		.color_space = RASTER_COLOR_SPACE_DEVICE_14,
		.keywords = _raster_color_space_device_14_keywords
	},

	{
		.color_space = RASTER_COLOR_SPACE_DEVICE_15,
		.keywords = _raster_color_space_device_15_keywords
	},

	{
		.color_space = 21,
		.keywords = NULL
	}
};

struct _raster_edge {
	const struct _raster_keyword *keywords;
	const raster_edge_t edge;
};

static const struct _raster_keyword _raster_edge_short_keywords[] =
{
	{
		.kw = "short",
		.min = 1
	},
	{
		.kw = NULL,
		.min = 0
	}
};

static const struct _raster_keyword _raster_edge_long_keywords[] =
{
	{
		.kw = "long",
		.min = 1
	},
	{
		.kw = NULL,
		.min = 0
	}
};

static const struct _raster_edge _raster_edge[] =
{
	{
		.edge = RASTER_EDGE_SHORT,
		.keywords = _raster_edge_short_keywords
	},

	{
		.edge = RASTER_EDGE_LONG,
		.keywords = _raster_edge_long_keywords
	},

	{
		.edge = 2,
		.keywords = NULL
	}
};

struct _raster_media_position {
	const struct _raster_keyword *keywords;
	const raster_media_position_t media_position;
};

static const struct _raster_keyword _raster_media_position_auto_keywords[] =
{
	{
		.kw = "auto",
		.min = 2
	},
	{
		.kw = NULL,
		.min = 0
	}
};

static const struct _raster_keyword _raster_media_position_main_keywords[] =
{
	{
		.kw = "main",
		.min = 4
	},
	{
		.kw = NULL,
		.min = 0
	}
};

static const struct _raster_keyword _raster_media_position_alternate_keywords[] =
{
	{
		.kw = "alternate",
		.min = 2
	},
	{
		.kw = "alt",
		.min = 2
	},
	{
		.kw = NULL,
		.min = 0
	}
};

static const struct _raster_keyword _raster_media_position_large_capacity_keywords[] =
{
	{
		.kw = "large_capacity",
		.min = 2
	},
	{
		.kw = "large-capacity",
		.min = 2
	},
	{
		.kw = "largecapacity",
		.min = 2
	},
	{
		.kw = "large",
		.min = 2
	},
	{
		.kw = NULL,
		.min = 0
	}
};

static const struct _raster_keyword _raster_media_position_manual_keywords[] =
{
	{
		.kw = "manual",
		.min = 3
	},
	{
		.kw = NULL,
		.min = 0
	}
};

static const struct _raster_keyword _raster_media_position_envelope_keywords[] =
{
	{
		.kw = "envelope",
		.min = 1
	},
	{
		.kw = "evn",
		.min = 1
	},
	{
		.kw = NULL,
		.min = 0
	}
};

static const struct _raster_keyword _raster_media_position_disc_keywords[] =
{
	{
		.kw = "disc",
		.min = 1
	},
	{
		.kw = NULL,
		.min = 0
	}
};

static const struct _raster_keyword _raster_media_position_photo_keywords[] =
{
	{
		.kw = "photo",
		.min = 1
	},
	{
		.kw = NULL,
		.min = 0
	}
};

static const struct _raster_keyword _raster_media_position_hagaki_keywords[] =
{
	{
		.kw = "hagaki",
		.min = 1
	},
	{
		.kw = NULL,
		.min = 0
	}
};

static const struct _raster_keyword _raster_media_position_main_roll_keywords[] =
{
	{
		.kw = "main_roll",
		.min = 5
	},
	{
		.kw = "main-roll",
		.min = 5
	},
	{
		.kw = "mainroll",
		.min = 5
	},
	{
		.kw = "mroll",
		.min = 2
	},
	{
		.kw = "roll_main",
		.min = 6
	},
	{
		.kw = "roll-main",
		.min = 6
	},
	{
		.kw = "rmain",
		.min = 2
	},
	{
		.kw = NULL,
		.min = 0
	}
};

static const struct _raster_keyword _raster_media_position_alternate_roll_keywords[] =
{
	{
		.kw = "alternate_roll",
		.min = 10
	},
	{
		.kw = "alternate-roll",
		.min = 10
	},
	{
		.kw = "alternateroll",
		.min = 10
	},
	{
		.kw = "alt_roll",
		.min = 4
	},
	{
		.kw = "alt-roll",
		.min = 4
	},
	{
		.kw = "altroll",
		.min = 4
	},
	{
		.kw = "rollalt",
		.min = 5
	},
	{
		.kw = "ralt",
		.min = 3
	},
	{
		.kw = NULL,
		.min = 0
	}
};

static const struct _raster_keyword _raster_media_position_top_keywords[] =
{
	{
		.kw = "top",
		.min = 2
	},
	{
		.kw = NULL,
		.min = 0
	}
};

static const struct _raster_keyword _raster_media_position_middle_keywords[] =
{
	{
		.kw = "middle",
		.min = 2
	},
	{
		.kw = NULL,
		.min = 0
	}
};

static const struct _raster_keyword _raster_media_position_bottom_keywords[] =
{
	{
		.kw = "bottom",
		.min = 2
	},
	{
		.kw = NULL,
		.min = 0
	}
};

static const struct _raster_keyword _raster_media_position_side_keywords[] =
{
	{
		.kw = "side",
		.min = 1
	},
	{
		.kw = NULL,
		.min = 0
	}
};

static const struct _raster_keyword _raster_media_position_left_keywords[] =
{
	{
		.kw = "left",
		.min = 2
	},
	{
		.kw = NULL,
		.min = 0
	}
};

static const struct _raster_keyword _raster_media_position_right_keywords[] =
{
	{
		.kw = "right",
		.min = 2
	},
	{
		.kw = NULL,
		.min = 0
	}
};

static const struct _raster_keyword _raster_media_position_center_keywords[] =
{
	{
		.kw = "center",
		.min = 1
	},
	{
		.kw = NULL,
		.min = 0
	}
};

static const struct _raster_keyword _raster_media_position_rear_keywords[] =
{
	{
		.kw = "rear",
		.min = 2
	},
	{
		.kw = NULL,
		.min = 0
	}
};

static const struct _raster_keyword _raster_media_position_bypass_tray_keywords[] =
{
	{
		.kw = "bypass_tray",
		.min = 2
	},
	{
		.kw = "bypass-tray",
		.min = 2
	},
	{
		.kw = "bypasstray",
		.min = 2
	},
	{
		.kw = "bypass",
		.min = 2
	},
	{
		.kw = "tray_bypass",
		.min = 7
	},
	{
		.kw = "tray-bypass",
		.min = 7
	},
	{
		.kw = "traybypass",
		.min = 6
	},
	{
		.kw = NULL,
		.min = 0
	}
};

static const struct _raster_keyword _raster_media_position_tray_1_keywords[] =
{
	{
		.kw = "tray_1",
		.min = 6
	},
	{
		.kw = "tray-1",
		.min = 6
	},
	{
		.kw = "tray1",
		.min = 5
	},
	{
		.kw = "t1",
		.min = 2
	},
	{
		.kw = NULL,
		.min = 0
	}
};

static const struct _raster_keyword _raster_media_position_tray_2_keywords[] =
{
	{
		.kw = "tray_2",
		.min = 6
	},
	{
		.kw = "tray-2",
		.min = 6
	},
	{
		.kw = "tray2",
		.min = 5
	},
	{
		.kw = "t2",
		.min = 2
	},
	{
		.kw = NULL,
		.min = 0
	}
};

static const struct _raster_keyword _raster_media_position_tray_3_keywords[] =
{
	{
		.kw = "tray_3",
		.min = 6
	},
	{
		.kw = "tray-3",
		.min = 6
	},
	{
		.kw = "tray3",
		.min = 5
	},
	{
		.kw = "t3",
		.min = 2
	},
	{
		.kw = NULL,
		.min = 0
	}
};

static const struct _raster_keyword _raster_media_position_tray_4_keywords[] =
{
	{
		.kw = "tray_4",
		.min = 6
	},
	{
		.kw = "tray-4",
		.min = 6
	},
	{
		.kw = "tray4",
		.min = 5
	},
	{
		.kw = "t4",
		.min = 2
	},
	{
		.kw = NULL,
		.min = 0
	}
};

static const struct _raster_keyword _raster_media_position_tray_5_keywords[] =
{
	{
		.kw = "tray_5",
		.min = 6
	},
	{
		.kw = "tray-5",
		.min = 6
	},
	{
		.kw = "tray5",
		.min = 5
	},
	{
		.kw = "t5",
		.min = 2
	},
	{
		.kw = NULL,
		.min = 0
	}
};

static const struct _raster_keyword _raster_media_position_tray_6_keywords[] =
{
	{
		.kw = "tray_6",
		.min = 6
	},
	{
		.kw = "tray-6",
		.min = 6
	},
	{
		.kw = "tray6",
		.min = 5
	},
	{
		.kw = "t6",
		.min = 2
	},
	{
		.kw = NULL,
		.min = 0
	}
};

static const struct _raster_keyword _raster_media_position_tray_7_keywords[] =
{
	{
		.kw = "tray_7",
		.min = 6
	},
	{
		.kw = "tray-7",
		.min = 6
	},
	{
		.kw = "tray7",
		.min = 5
	},
	{
		.kw = "t7",
		.min = 2
	},
	{
		.kw = NULL,
		.min = 0
	}
};

static const struct _raster_keyword _raster_media_position_tray_8_keywords[] =
{
	{
		.kw = "tray_8",
		.min = 6
	},
	{
		.kw = "tray-8",
		.min = 6
	},
	{
		.kw = "tray8",
		.min = 5
	},
	{
		.kw = "t8",
		.min = 2
	},
	{
		.kw = NULL,
		.min = 0
	}
};

static const struct _raster_keyword _raster_media_position_tray_9_keywords[] =
{
	{
		.kw = "tray_9",
		.min = 6
	},
	{
		.kw = "tray-9",
		.min = 6
	},
	{
		.kw = "tray9",
		.min = 5
	},
	{
		.kw = "t9",
		.min = 2
	},
	{
		.kw = NULL,
		.min = 0
	}
};

static const struct _raster_keyword _raster_media_position_tray_10_keywords[] =
{
	{
		.kw = "tray_10",
		.min = 7
	},
	{
		.kw = "tray-10",
		.min = 7
	},
	{
		.kw = "tray10",
		.min = 6
	},
	{
		.kw = "t10",
		.min = 3
	},
	{
		.kw = "tray_a",
		.min = 6
	},
	{
		.kw = "tray-a",
		.min = 6
	},
	{
		.kw = "traya",
		.min = 5
	},
	{
		.kw = "ta",
		.min = 2
	},
	{
		.kw = NULL,
		.min = 0
	}
};

static const struct _raster_keyword _raster_media_position_tray_11_keywords[] =
{
	{
		.kw = "tray_11",
		.min = 7
	},
	{
		.kw = "tray-11",
		.min = 7
	},
	{
		.kw = "tray11",
		.min = 6
	},
	{
		.kw = "t11",
		.min = 3
	},
	{
		.kw = "tray_b",
		.min = 6
	},
	{
		.kw = "tray-b",
		.min = 6
	},
	{
		.kw = "trayb",
		.min = 5
	},
	{
		.kw = "tb",
		.min = 2
	},
	{
		.kw = NULL,
		.min = 0
	}
};

static const struct _raster_keyword _raster_media_position_tray_12_keywords[] =
{
	{
		.kw = "tray_12",
		.min = 7
	},
	{
		.kw = "tray-12",
		.min = 7
	},
	{
		.kw = "tray12",
		.min = 6
	},
	{
		.kw = "t12",
		.min = 3
	},
	{
		.kw = "tray_c",
		.min = 6
	},
	{
		.kw = "tray-c",
		.min = 6
	},
	{
		.kw = "trayc",
		.min = 5
	},
	{
		.kw = "tc",
		.min = 2
	},
	{
		.kw = NULL,
		.min = 0
	}
};

static const struct _raster_keyword _raster_media_position_tray_13_keywords[] =
{
	{
		.kw = "tray_13",
		.min = 7
	},
	{
		.kw = "tray-13",
		.min = 7
	},
	{
		.kw = "tray13",
		.min = 6
	},
	{
		.kw = "t13",
		.min = 3
	},
	{
		.kw = "tray_d",
		.min = 6
	},
	{
		.kw = "tray-d",
		.min = 6
	},
	{
		.kw = "trayd",
		.min = 5
	},
	{
		.kw = "td",
		.min = 2
	},
	{
		.kw = NULL,
		.min = 0
	}
};

static const struct _raster_keyword _raster_media_position_tray_14_keywords[] =
{
	{
		.kw = "tray_14",
		.min = 7
	},
	{
		.kw = "tray-14",
		.min = 7
	},
	{
		.kw = "tray14",
		.min = 6
	},
	{
		.kw = "t14",
		.min = 3
	},
	{
		.kw = "tray_e",
		.min = 6
	},
	{
		.kw = "tray-e",
		.min = 6
	},
	{
		.kw = "trayd",
		.min = 5
	},
	{
		.kw = "te",
		.min = 2
	},
	{
		.kw = NULL,
		.min = 0
	}
};

static const struct _raster_keyword _raster_media_position_tray_15_keywords[] =
{
	{
		.kw = "tray_15",
		.min = 7
	},
	{
		.kw = "tray-15",
		.min = 7
	},
	{
		.kw = "tray15",
		.min = 6
	},
	{
		.kw = "t15",
		.min = 3
	},
	{
		.kw = "tray_f",
		.min = 6
	},
	{
		.kw = "tray-f",
		.min = 6
	},
	{
		.kw = "trayf",
		.min = 5
	},
	{
		.kw = "tf",
		.min = 2
	},
	{
		.kw = NULL,
		.min = 0
	}
};

static const struct _raster_keyword _raster_media_position_tray_16_keywords[] =
{
	{
		.kw = "tray_16",
		.min = 7
	},
	{
		.kw = "tray-16",
		.min = 7
	},
	{
		.kw = "tray16",
		.min = 6
	},
	{
		.kw = "t16",
		.min = 3
	},
	{
		.kw = "tray_g",
		.min = 6
	},
	{
		.kw = "tray-g",
		.min = 6
	},
	{
		.kw = "trayg",
		.min = 5
	},
	{
		.kw = "tg",
		.min = 2
	},
	{
		.kw = NULL,
		.min = 0
	}
};

static const struct _raster_keyword _raster_media_position_tray_17_keywords[] =
{
	{
		.kw = "tray_17",
		.min = 7
	},
	{
		.kw = "tray-17",
		.min = 7
	},
	{
		.kw = "tray17",
		.min = 6
	},
	{
		.kw = "t17",
		.min = 3
	},
	{
		.kw = "tray_h",
		.min = 6
	},
	{
		.kw = "tray-h",
		.min = 6
	},
	{
		.kw = "trayh",
		.min = 5
	},
	{
		.kw = "th",
		.min = 2
	},
	{
		.kw = NULL,
		.min = 0
	}
};

static const struct _raster_keyword _raster_media_position_tray_18_keywords[] =
{
	{
		.kw = "tray_18",
		.min = 7
	},
	{
		.kw = "tray-18",
		.min = 7
	},
	{
		.kw = "tray18",
		.min = 6
	},
	{
		.kw = "t18",
		.min = 3
	},
	{
		.kw = "tray_i",
		.min = 6
	},
	{
		.kw = "tray-i",
		.min = 6
	},
	{
		.kw = "trayi",
		.min = 5
	},
	{
		.kw = "ti",
		.min = 2
	},
	{
		.kw = NULL,
		.min = 0
	}
};

static const struct _raster_keyword _raster_media_position_tray_19_keywords[] =
{
	{
		.kw = "tray_19",
		.min = 7
	},
	{
		.kw = "tray-19",
		.min = 7
	},
	{
		.kw = "tray19",
		.min = 6
	},
	{
		.kw = "t19",
		.min = 3
	},
	{
		.kw = "tray_j",
		.min = 6
	},
	{
		.kw = "tray-j",
		.min = 6
	},
	{
		.kw = "trayj",
		.min = 5
	},
	{
		.kw = "tj",
		.min = 2
	},
	{
		.kw = NULL,
		.min = 0
	}
};

static const struct _raster_keyword _raster_media_position_tray_20_keywords[] =
{
	{
		.kw = "tray_20",
		.min = 7
	},
	{
		.kw = "tray-20",
		.min = 7
	},
	{
		.kw = "tray20",
		.min = 6
	},
	{
		.kw = "t20",
		.min = 3
	},
	{
		.kw = "tray_k",
		.min = 6
	},
	{
		.kw = "tray-k",
		.min = 6
	},
	{
		.kw = "trayk",
		.min = 5
	},
	{
		.kw = "tk",
		.min = 2
	},
	{
		.kw = NULL,
		.min = 0
	}
};

static const struct _raster_keyword _raster_media_position_roll_1_keywords[] =
{
	{
		.kw = "roll_1",
		.min = 6
	},
	{
		.kw = "roll-1",
		.min = 6
	},
	{
		.kw = "roll1",
		.min = 5
	},
	{
		.kw = "r1",
		.min = 2
	},
	{
		.kw = NULL,
		.min = 0
	}
};

static const struct _raster_keyword _raster_media_position_roll_2_keywords[] =
{
	{
		.kw = "roll_2",
		.min = 6
	},
	{
		.kw = "roll-2",
		.min = 6
	},
	{
		.kw = "roll2",
		.min = 5
	},
	{
		.kw = "r2",
		.min = 2
	},
	{
		.kw = NULL,
		.min = 0
	}
};

static const struct _raster_keyword _raster_media_position_roll_3_keywords[] =
{
	{
		.kw = "roll_3",
		.min = 6
	},
	{
		.kw = "roll-3",
		.min = 6
	},
	{
		.kw = "roll3",
		.min = 5
	},
	{
		.kw = "r3",
		.min = 2
	},
	{
		.kw = NULL,
		.min = 0
	}
};

static const struct _raster_keyword _raster_media_position_roll_4_keywords[] =
{
	{
		.kw = "roll_4",
		.min = 6
	},
	{
		.kw = "roll-4",
		.min = 6
	},
	{
		.kw = "roll4",
		.min = 5
	},
	{
		.kw = "r4",
		.min = 2
	},
	{
		.kw = NULL,
		.min = 0
	}
};

static const struct _raster_keyword _raster_media_position_roll_5_keywords[] =
{
	{
		.kw = "roll_5",
		.min = 6
	},
	{
		.kw = "roll-5",
		.min = 6
	},
	{
		.kw = "roll5",
		.min = 5
	},
	{
		.kw = "r5",
		.min = 2
	},
	{
		.kw = NULL,
		.min = 0
	}
};

static const struct _raster_keyword _raster_media_position_roll_6_keywords[] =
{
	{
		.kw = "roll_6",
		.min = 6
	},
	{
		.kw = "roll-6",
		.min = 6
	},
	{
		.kw = "roll7",
		.min = 5
	},
	{
		.kw = "r6",
		.min = 2
	},
	{
		.kw = NULL,
		.min = 0
	}
};

static const struct _raster_keyword _raster_media_position_roll_7_keywords[] =
{
	{
		.kw = "roll_7",
		.min = 6
	},
	{
		.kw = "roll-7",
		.min = 6
	},
	{
		.kw = "roll7",
		.min = 5
	},
	{
		.kw = "r7",
		.min = 2
	},
	{
		.kw = NULL,
		.min = 0
	}
};

static const struct _raster_keyword _raster_media_position_roll_8_keywords[] =
{
	{
		.kw = "roll_8",
		.min = 6
	},
	{
		.kw = "roll-8",
		.min = 6
	},
	{
		.kw = "roll8",
		.min = 5
	},
	{
		.kw = "r8",
		.min = 2
	},
	{
		.kw = NULL,
		.min = 0
	}
};

static const struct _raster_keyword _raster_media_position_roll_9_keywords[] =
{
	{
		.kw = "roll_9",
		.min = 6
	},
	{
		.kw = "roll-9",
		.min = 6
	},
	{
		.kw = "roll9",
		.min = 5
	},
	{
		.kw = "r9",
		.min = 2
	},
	{
		.kw = NULL,
		.min = 0
	}
};

static const struct _raster_keyword _raster_media_position_roll_10_keywords[] =
{
	{
		.kw = "roll_10",
		.min = 7
	},
	{
		.kw = "roll-10",
		.min = 7
	},
	{
		.kw = "roll10",
		.min = 6
	},
	{
		.kw = "r10",
		.min = 3
	},
	{
		.kw = "roll_a",
		.min = 7
	},
	{
		.kw = "roll-a",
		.min = 7
	},
	{
		.kw = "rolla",
		.min = 6
	},
	{
		.kw = "ra",
		.min = 3
	},
	{
		.kw = NULL,
		.min = 0
	}
};

static const struct _raster_media_position _raster_media_position[] =
{
	{
		.media_position = RASTER_MEDIA_POSITION_AUTO,
		.keywords = _raster_media_position_auto_keywords
	},

	{
		.media_position = RASTER_MEDIA_POSITION_MAIN,
		.keywords = _raster_media_position_main_keywords
	},

	{
		.media_position = RASTER_MEDIA_POSITION_ALTERNATE,
		.keywords = _raster_media_position_alternate_keywords
	},

	{
		.media_position = RASTER_MEDIA_POSITION_LARGE_CAPACITY,
		.keywords = _raster_media_position_large_capacity_keywords
	},

	{
		.media_position = RASTER_MEDIA_POSITION_MANUAL,
		.keywords = _raster_media_position_manual_keywords
	},

	{
		.media_position = RASTER_MEDIA_POSITION_ENVELOPE,
		.keywords = _raster_media_position_envelope_keywords
	},

	{
		.media_position = RASTER_MEDIA_POSITION_DISC,
		.keywords = _raster_media_position_disc_keywords
	},

	{
		.media_position = RASTER_MEDIA_POSITION_PHOTO,
		.keywords = _raster_media_position_photo_keywords
	},

	{
		.media_position = RASTER_MEDIA_POSITION_HAGAKI,
		.keywords = _raster_media_position_hagaki_keywords
	},

	{
		.media_position = RASTER_MEDIA_POSITION_MAIN_ROLL,
		.keywords = _raster_media_position_main_roll_keywords
	},

	{
		.media_position = RASTER_MEDIA_POSITION_ALTERNATE_ROLL,
		.keywords = _raster_media_position_alternate_roll_keywords
	},

	{
		.media_position = RASTER_MEDIA_POSITION_TOP,
		.keywords = _raster_media_position_top_keywords
	},

	{
		.media_position = RASTER_MEDIA_POSITION_MIDDLE,
		.keywords = _raster_media_position_middle_keywords
	},

	{
		.media_position = RASTER_MEDIA_POSITION_BOTTOM,
		.keywords = _raster_media_position_bottom_keywords
	},

	{
		.media_position = RASTER_MEDIA_POSITION_SIDE,
		.keywords = _raster_media_position_side_keywords
	},

	{
		.media_position = RASTER_MEDIA_POSITION_LEFT,
		.keywords = _raster_media_position_left_keywords
	},

	{
		.media_position = RASTER_MEDIA_POSITION_RIGHT,
		.keywords = _raster_media_position_right_keywords
	},

	{
		.media_position = RASTER_MEDIA_POSITION_CENTER,
		.keywords = _raster_media_position_center_keywords
	},

	{
		.media_position = RASTER_MEDIA_POSITION_REAR,
		.keywords = _raster_media_position_rear_keywords
	},

	{
		.media_position = RASTER_MEDIA_POSITION_BYPASS_TRAY,
		.keywords = _raster_media_position_bypass_tray_keywords
	},

	{
		.media_position = RASTER_MEDIA_POSITION_TRAY_1,
		.keywords = _raster_media_position_tray_1_keywords
	},

	{
		.media_position = RASTER_MEDIA_POSITION_TRAY_2,
		.keywords = _raster_media_position_tray_2_keywords
	},

	{
		.media_position = RASTER_MEDIA_POSITION_TRAY_3,
		.keywords = _raster_media_position_tray_3_keywords
	},

	{
		.media_position = RASTER_MEDIA_POSITION_TRAY_4,
		.keywords = _raster_media_position_tray_4_keywords
	},

	{
		.media_position = RASTER_MEDIA_POSITION_TRAY_5,
		.keywords = _raster_media_position_tray_5_keywords
	},

	{
		.media_position = RASTER_MEDIA_POSITION_TRAY_6,
		.keywords = _raster_media_position_tray_6_keywords
	},

	{
		.media_position = RASTER_MEDIA_POSITION_TRAY_7,
		.keywords = _raster_media_position_tray_7_keywords
	},

	{
		.media_position = RASTER_MEDIA_POSITION_TRAY_8,
		.keywords = _raster_media_position_tray_8_keywords
	},

	{
		.media_position = RASTER_MEDIA_POSITION_TRAY_9,
		.keywords = _raster_media_position_tray_9_keywords
	},

	{
		.media_position = RASTER_MEDIA_POSITION_TRAY_10,
		.keywords = _raster_media_position_tray_10_keywords
	},

	{
		.media_position = RASTER_MEDIA_POSITION_TRAY_11,
		.keywords = _raster_media_position_tray_11_keywords
	},

	{
		.media_position = RASTER_MEDIA_POSITION_TRAY_12,
		.keywords = _raster_media_position_tray_12_keywords
	},

	{
		.media_position = RASTER_MEDIA_POSITION_TRAY_13,
		.keywords = _raster_media_position_tray_13_keywords
	},

	{
		.media_position = RASTER_MEDIA_POSITION_TRAY_14,
		.keywords = _raster_media_position_tray_14_keywords
	},

	{
		.media_position = RASTER_MEDIA_POSITION_TRAY_15,
		.keywords = _raster_media_position_tray_15_keywords
	},

	{
		.media_position = RASTER_MEDIA_POSITION_TRAY_16,
		.keywords = _raster_media_position_tray_16_keywords
	},

	{
		.media_position = RASTER_MEDIA_POSITION_TRAY_17,
		.keywords = _raster_media_position_tray_17_keywords
	},

	{
		.media_position = RASTER_MEDIA_POSITION_TRAY_18,
		.keywords = _raster_media_position_tray_18_keywords
	},

	{
		.media_position = RASTER_MEDIA_POSITION_TRAY_19,
		.keywords = _raster_media_position_tray_19_keywords
	},

	{
		.media_position = RASTER_MEDIA_POSITION_TRAY_20,
		.keywords = _raster_media_position_tray_20_keywords
	},

	{
		.media_position = RASTER_MEDIA_POSITION_ROLL_1,
		.keywords = _raster_media_position_roll_1_keywords
	},

	{
		.media_position = RASTER_MEDIA_POSITION_ROLL_2,
		.keywords = _raster_media_position_roll_2_keywords
	},

	{
		.media_position = RASTER_MEDIA_POSITION_ROLL_3,
		.keywords = _raster_media_position_roll_3_keywords
	},

	{
		.media_position = RASTER_MEDIA_POSITION_ROLL_4,
		.keywords = _raster_media_position_roll_4_keywords
	},

	{
		.media_position = RASTER_MEDIA_POSITION_ROLL_5,
		.keywords = _raster_media_position_roll_5_keywords
	},

	{
		.media_position = RASTER_MEDIA_POSITION_ROLL_6,
		.keywords = _raster_media_position_roll_6_keywords
	},

	{
		.media_position = RASTER_MEDIA_POSITION_ROLL_7,
		.keywords = _raster_media_position_roll_7_keywords
	},

	{
		.media_position = RASTER_MEDIA_POSITION_ROLL_8,
		.keywords = _raster_media_position_roll_8_keywords
	},

	{
		.media_position = RASTER_MEDIA_POSITION_ROLL_9,
		.keywords = _raster_media_position_roll_9_keywords
	},

	{
		.media_position = RASTER_MEDIA_POSITION_ROLL_10,
		.keywords = _raster_media_position_roll_10_keywords
	},

	{
		.media_position = 50,
		.keywords = NULL
	}
};

struct _raster_orientation {
	const struct _raster_keyword *keywords;
	const raster_orientation_t orientation;
};

static const struct _raster_keyword _raster_orientation_portrait_keywords[] =
{
	{
		.kw = "portrait",
		.min = 1
	},
	{
		.kw = NULL,
		.min = 0
	}
};

static const struct _raster_keyword _raster_orientation_landscape_keywords[] =
{
	{
		.kw = "landscape",
		.min = 1
	},
	{
		.kw = NULL,
		.min = 0
	}
};

static const struct _raster_keyword _raster_orientation_reverse_portrait_keywords[] =
{
	{
		.kw = "reverse_portrait",
		.min = 9
	},
	{
		.kw = "reverse-portrait",
		.min = 9
	},
	{
		.kw = "rev_portrait",
		.min = 5
	},
	{
		.kw = "rev-portrait",
		.min = 5
	},
	{
		.kw = "revportrait",
		.min = 4
	},
	{
		.kw = "rportrait",
		.min = 2
	},
	{
		.kw = NULL,
		.min = 0
	}
};

static const struct _raster_keyword _raster_orientation_reverse_landscape_keywords[] =
{
	{
		.kw = "reverse_landscape",
		.min = 9
	},
	{
		.kw = "reverse-landscape",
		.min = 9
	},
	{
		.kw = "rev_landscape",
		.min = 5
	},
	{
		.kw = "rev-landscape",
		.min = 5
	},
	{
		.kw = "revlandscape",
		.min = 4
	},
	{
		.kw = "rlandscape",
		.min = 2
	},
	{
		.kw = "seascape",
		.min = 1
	},
	{
		.kw = NULL,
		.min = 0
	}
};

static const struct _raster_orientation _raster_orientation[] =
{
	{
		.orientation = RASTER_ORIENTATION_PORTRAIT,
		.keywords = _raster_orientation_portrait_keywords
	},

	{
		.orientation = RASTER_ORIENTATION_LANDSCAPE,
		.keywords = _raster_orientation_landscape_keywords
	},

	{
		.orientation = RASTER_ORIENTATION_REVERSE_PORTRAIT,
		.keywords = _raster_orientation_reverse_portrait_keywords
	},

	{
		.orientation = RASTER_ORIENTATION_REVERSE_LANDSCAPE,
		.keywords = _raster_orientation_reverse_landscape_keywords
	},

	{
		.orientation = 4,
		.keywords = NULL
	}
};

struct _raster_print_quality {
	const struct _raster_keyword *keywords;
	const raster_print_quality_t print_quality;
};

static const struct _raster_keyword _raster_print_quality_default_keywords[] =
{
	{
		.kw = "default",
		.min = 2
	},
	{
		.kw = NULL,
		.min = 0
	}
};

static const struct _raster_keyword _raster_print_quality_draft_keywords[] =
{
	{
		.kw = "draft",
		.min = 2
	},
	{
		.kw = NULL,
		.min = 0
	}
};

static const struct _raster_keyword _raster_print_quality_normal_keywords[] =
{
	{
		.kw = "normal",
		.min = 1
	},
	{
		.kw = NULL,
		.min = 0
	}
};

static const struct _raster_keyword _raster_print_quality_high_keywords[] =
{
	{
		.kw = "high",
		.min = 1
	},
	{
		.kw = NULL,
		.min = 0
	}
};

static const struct _raster_print_quality _raster_print_quality[] =
{
	{
		.print_quality = RASTER_QUALITY_DEFAULT,
		.keywords = _raster_print_quality_default_keywords
	},

	{
		.print_quality = RASTER_QUALITY_DRAFT,
		.keywords = _raster_print_quality_draft_keywords
	},

	{
		.print_quality = RASTER_QUALITY_NORMAL,
		.keywords = _raster_print_quality_normal_keywords
	},

	{
		.print_quality = RASTER_QUALITY_HIGH,
		.keywords = _raster_print_quality_high_keywords
	},

	{
		.print_quality = 4,
		.keywords = NULL
	}
};

struct _raster_when {
	const struct _raster_keyword *keywords;
	const raster_when_t when;
};

static const struct _raster_keyword _raster_when_never_keywords[] =
{
	{
		.kw = "never",
		.min = 1
	},
	{
		.kw = NULL,
		.min = 0
	}
};

static const struct _raster_keyword _raster_when_after_document_keywords[] =
{
	{
		.kw = "after_document",
		.min = 7
	},
	{
		.kw = "after-document",
		.min = 7
	},
	{
		.kw = "afterdocument",
		.min = 6
	},
	{
		.kw = "document",
		.min = 1
	},
	{
		.kw = NULL,
		.min = 0
	}
};

static const struct _raster_keyword _raster_when_after_job_keywords[] =
{
	{
		.kw = "after_job",
		.min = 7
	},
	{
		.kw = "after-job",
		.min = 7
	},
	{
		.kw = "afterjob",
		.min = 6
	},
	{
		.kw = "job",
		.min = 1
	},
	{
		.kw = NULL,
		.min = 0
	}
};

static const struct _raster_keyword _raster_when_after_set_keywords[] =
{
	{
		.kw = "after_set",
		.min = 7
	},
	{
		.kw = "after-set",
		.min = 7
	},
	{
		.kw = "afterset",
		.min = 6
	},
	{
		.kw = "set",
		.min = 1
	},
	{
		.kw = NULL,
		.min = 0
	}
};

static const struct _raster_keyword _raster_when_after_page_keywords[] =
{
	{
		.kw = "after_page",
		.min = 7
	},
	{
		.kw = "after-page",
		.min = 7
	},
	{
		.kw = "afterpage",
		.min = 6
	},
	{
		.kw = "page",
		.min = 1
	},
	{
		.kw = NULL,
		.min = 0
	}
};

static const struct _raster_when _raster_when[] =
{
	{
		.when = RASTER_WHEN_NEVER,
		.keywords = _raster_when_never_keywords
	},

	{
		.when = RASTER_WHEN_AFTER_DOCUMENT,
		.keywords = _raster_when_after_document_keywords
	},

	{
		.when = RASTER_WHEN_AFTER_JOB,
		.keywords = _raster_when_after_job_keywords
	},

	{
		.when = RASTER_WHEN_AFTER_SET,
		.keywords = _raster_when_after_set_keywords
	},

	{
		.when = RASTER_WHEN_AFTER_PAGE,
		.keywords = _raster_when_after_page_keywords
	},

	{
		.when = 5,
		.keywords = NULL
	}
};

struct _raster_xform {
	const struct _raster_keyword *keywords;
	const raster_xform_t xform;
};

static const struct _raster_keyword _raster_xform_reversed_keywords[] =
{
	{
		.kw = "reversed",
		.min = 1
	},
	{
		.kw = NULL,
		.min = 0
	}
};

static const struct _raster_keyword _raster_xform_unknown_keywords[] =
{
	{
		.kw = "unknown",
		.min = 1
	},
	{
		.kw = NULL,
		.min = 0
	}
};

static const struct _raster_keyword _raster_xform_normal_keywords[] =
{
	{
		.kw = "normal",
		.min = 1
	},
	{
		.kw = NULL,
		.min = 0
	}
};

static const struct _raster_xform _raster_xform[] =
{
	{
		.xform = RASTER_XFORM_REVERSED,
		.keywords = _raster_xform_reversed_keywords
	},

	{
		.xform = RASTER_XFORM_UNKNOWN,
		.keywords = _raster_xform_unknown_keywords
	},

	{
		.xform = RASTER_XFORM_NORMAL,
		.keywords = _raster_xform_normal_keywords
	},

	{
		.xform = 3,
		.keywords = NULL
	}
};

struct _raster_sides {
	const struct _raster_keyword *keywords;
	const raster_sides_t sides;
};

static const struct _raster_keyword _raster_sides_one_sided_keywords[] =
{
	{
		.kw = "one_sided",
		.min = 1
	},
	{
		.kw = "one-sided",
		.min = 1
	},
	{
		.kw = NULL,
		.min = 0
	}
};

static const struct _raster_keyword _raster_sides_two_sided_long_edge_keywords[] =
{
	{
		.kw = "two_sided_long_edge",
		.min = 11
	},
	{
		.kw = "two-sided-long-edge",
		.min = 11
	},
	{
		.kw = "long_edge",
		.min = 1
	},
	{
		.kw = "long-edge",
		.min = 1
	},
	{
		.kw = "longedge",
		.min = 1
	},
	{
		.kw = NULL,
		.min = 0
	}
};

static const struct _raster_keyword _raster_sides_two_sided_short_edge_keywords[] =
{
	{
		.kw = "two_sided_short_edge",
		.min = 11
	},
	{
		.kw = "two-sided-short-edge",
		.min = 11
	},
	{
		.kw = "short_edge",
		.min = 1
	},
	{
		.kw = "short-edge",
		.min = 1
	},
	{
		.kw = "shortedge",
		.min = 1
	},
	{
		.kw = NULL,
		.min = 0
	}
};

static const struct _raster_sides _raster_sides[] =
{
	{
		.sides = RASTER_SIDES_ONE_SIDED,
		.keywords = _raster_sides_one_sided_keywords
	},

	{
		.sides = RASTER_SIDES_TWO_SIDED_LONG_EDGE,
		.keywords = _raster_sides_two_sided_long_edge_keywords
	},

	{
		.sides = RASTER_SIDES_TWO_SIDED_SHORT_EDGE,
		.keywords = _raster_sides_two_sided_short_edge_keywords
	},

	{
		.sides = 3,
		.keywords = NULL
	}
};

struct _raster_sheetback {
	const struct _raster_keyword *keywords;
	const raster_sheetback_t sheetback;
};

static const struct _raster_keyword _raster_sheetback_normal_keywords[] =
{
	{
		.kw = "normal",
		.min = 1
	},
	{
		.kw = NULL,
		.min = 0
	}
};

static const struct _raster_keyword _raster_sheetback_flipped_keywords[] =
{
	{
		.kw = "flipped",
		.min = 1
	},
	{
		.kw = NULL,
		.min = 0
	}
};

static const struct _raster_keyword _raster_sheetback_rotated_keywords[] =
{
	{
		.kw = "rotated",
		.min = 1
	},
	{
		.kw = NULL,
		.min = 0
	}
};

static const struct _raster_keyword _raster_sheetback_manual_keywords[] =
{
	{
		.kw = "manual",
		.min = 1
	},
	{
		.kw = NULL,
		.min = 0
	}
};

static const struct _raster_sheetback _raster_sheetback[] =
{
	{
		.sheetback = RASTER_SHEETBACK_NORMAL,
		.keywords = _raster_sheetback_normal_keywords
	},

	{
		.sheetback = RASTER_SHEETBACK_FLIPPED,
		.keywords = _raster_sheetback_flipped_keywords
	},

	{
		.sheetback = RASTER_SHEETBACK_ROTATED,
		.keywords = _raster_sheetback_rotated_keywords
	},

	{
		.sheetback = RASTER_SHEETBACK_MANUAL,
		.keywords = _raster_sheetback_manual_keywords
	},

	{
		.sheetback = 4,
		.keywords = NULL
	}
};

struct _raster_document_type {
	const struct _raster_keyword *keywords;
	const char *ipp_doc_type_string;
	const raster_document_type_t document_type;
	const raster_color_space_t color_space;
	const uint32_t bits_per_pixel;
	const uint32_t bits_per_color;
	const uint32_t num_colors;
};

/*
 *  For these structs, the first entry is the Table12 value
 *      the second is the pwg-raster-document-type-supported value
 *
 */

static const struct _raster_keyword _raster_document_type_black_1_keywords[] =
{
	{
		.kw = "Black_1",
		.min = 6
	},
	{
		.kw = "black_1",
		.min = 6
	},
	{
		.kw = "black-1",
		.min = 6
	},
	{
		.kw = "black1",
		.min = 6
	},
	{
		.kw = "k1",
		.min = 2
	},
	{
		.kw = "k-1",
		.min = 3
	},
	{
		.kw = "k_1",
		.min = 3
	},
	{
		.kw = NULL,
		.min = 0
	}
};

static const struct _raster_keyword _raster_document_type_sgray_1_keywords[] =
{
	{
		.kw = "Sgray_1",
		.min = 7
	},
	{
		.kw = "sgray_1",
		.min = 7
	},
	{
		.kw = "sgray-1",
		.min = 7
	},
	{
		.kw = "sgray1",
		.min = 6
	},
	{
		.kw = "s2-1",
		.min = 4
	},
	{
		.kw = "sw1",
		.min = 3
	},
	{
		.kw = NULL,
		.min = 0
	}
};

static const struct _raster_keyword _raster_document_type_adobe_rgb_8_keywords[] =
{
	{
		.kw = "AdobeRgb_8",
		.min = 10
	},
	{
		.kw = "adobe-rgb_8",
		.min = 11
	},
	{
		.kw = "adobe_rgb_8",
		.min = 11
	},
	{
		.kw = "adobe-rgb-8",
		.min = 11
	},
	{
		.kw = "adobe_rgb-8",
		.min = 11
	},
	{
		.kw = "adobergb8",
		.min = 9
	},
	{
		.kw = "adobe8",
		.min = 6
	},
	{
		.kw = NULL,
		.min = 0
	}
};

static const struct _raster_keyword _raster_document_type_black_8_keywords[] =
{
	{
		.kw = "Black_8",
		.min = 6
	},
	{
		.kw = "black_8",
		.min = 6
	},
	{
		.kw = "black-8",
		.min = 6
	},
	{
		.kw = "black8",
		.min = 6
	},
	{
		.kw = "k_8",
		.min = 3
	},
	{
		.kw = "k-8",
		.min = 3
	},
	{
		.kw = "k8",
		.min = 2
	},
	{
		.kw = NULL,
		.min = 0
	}
};

static const struct _raster_keyword _raster_document_type_cmyk_8_keywords[] =
{
	{
		.kw = "Cmyk_8",
		.min = 6
	},
	{
		.kw = "cmyk_8",
		.min = 6
	},
	{
		.kw = "cmyk-8",
		.min = 6
	},
	{
		.kw = "cmyk8",
		.min = 5
	},
	{
		.kw = NULL,
		.min = 0
	}
};

static const struct _raster_keyword _raster_document_type_device_1_8_keywords[] = {
	{
		.kw = "Device1_8",
		.min = 9
	},
	{
		.kw = "device1_8",
		.min = 9
	},
	{
		.kw = "device1-8",
		.min = 9
	},
	{
		.kw = "device18",
		.min = 8
	},
	{
		.kw = "device_1_8",
		.min = 10
	},
	{
		.kw = "device-1-8",
		.min = 10
	},
	{
		.kw = "device-1_8",
		.min = 10
	},
	{
		.kw = "device_1-8",
		.min = 10
	},
	{
		.kw = "dev1_8",
		.min = 6
	},
	{
		.kw = "dev1-8",
		.min = 6
	},
	{
		.kw = "dev18",
		.min = 5
	},
	{
		.kw = "dev_1_8",
		.min = 7
	},
	{
		.kw = "dev-1-8",
		.min = 7
	},
	{
		.kw = "dev-1_8",
		.min = 7
	},
	{
		.kw = "dev_1-8",
		.min = 7
	},
	{
		.kw = NULL,
		.min = 0
	}
};

static const struct _raster_keyword _raster_document_type_device_2_8_keywords[] = {
	{
		.kw = "Device2_8",
		.min = 9
	},
	{
		.kw = "device2_8",
		.min = 9
	},
	{
		.kw = "device2-8",
		.min = 9
	},
	{
		.kw = "device28",
		.min = 8
	},
	{
		.kw = "device_2_8",
		.min = 10
	},
	{
		.kw = "device-2-8",
		.min = 10
	},
	{
		.kw = "device-2_8",
		.min = 10
	},
	{
		.kw = "device_2-8",
		.min = 10
	},
	{
		.kw = "dev2_8",
		.min = 6
	},
	{
		.kw = "dev2-8",
		.min = 6
	},
	{
		.kw = "dev28",
		.min = 5
	},
	{
		.kw = "dev_2_8",
		.min = 7
	},
	{
		.kw = "dev-2-8",
		.min = 7
	},
	{
		.kw = "dev-2_8",
		.min = 7
	},
	{
		.kw = "dev_2-8",
		.min = 7
	},
	{
		.kw = NULL,
		.min = 0
	}
};

static const struct _raster_keyword _raster_document_type_device_3_8_keywords[] = {
	{
		.kw = "Device3_8",
		.min = 9
	},
	{
		.kw = "device3_8",
		.min = 9
	},
	{
		.kw = "device3-8",
		.min = 9
	},
	{
		.kw = "device38",
		.min = 8
	},
	{
		.kw = "device_3_8",
		.min = 10
	},
	{
		.kw = "device-3-8",
		.min = 10
	},
	{
		.kw = "device-3_8",
		.min = 10
	},
	{
		.kw = "device_3-8",
		.min = 10
	},
	{
		.kw = "dev3_8",
		.min = 6
	},
	{
		.kw = "dev3-8",
		.min = 6
	},
	{
		.kw = "dev38",
		.min = 5
	},
	{
		.kw = "dev_3_8",
		.min = 7
	},
	{
		.kw = "dev-3-8",
		.min = 7
	},
	{
		.kw = "dev-3_8",
		.min = 7
	},
	{
		.kw = "dev_3-8",
		.min = 7
	},
	{
		.kw = NULL,
		.min = 0
	}
};

static const struct _raster_keyword _raster_document_type_device_4_8_keywords[] = {
	{
		.kw = "Device4_8",
		.min = 9
	},
	{
		.kw = "device4_8",
		.min = 9
	},
	{
		.kw = "device4-8",
		.min = 9
	},
	{
		.kw = "device48",
		.min = 8
	},
	{
		.kw = "device_4_8",
		.min = 10
	},
	{
		.kw = "device-4-8",
		.min = 10
	},
	{
		.kw = "device-4_8",
		.min = 10
	},
	{
		.kw = "device_4-8",
		.min = 10
	},
	{
		.kw = "dev4_8",
		.min = 6
	},
	{
		.kw = "dev4-8",
		.min = 6
	},
	{
		.kw = "dev48",
		.min = 5
	},
	{
		.kw = "dev_4_8",
		.min = 7
	},
	{
		.kw = "dev-4-8",
		.min = 7
	},
	{
		.kw = "dev-4_8",
		.min = 7
	},
	{
		.kw = "dev_4-8",
		.min = 7
	},
	{
		.kw = NULL,
		.min = 0
	}
};

static const struct _raster_keyword _raster_document_type_device_5_8_keywords[] = {
	{
		.kw = "Device5_8",
		.min = 9
	},
	{
		.kw = "device5_8",
		.min = 9
	},
	{
		.kw = "device5-8",
		.min = 9
	},
	{
		.kw = "device58",
		.min = 8
	},
	{
		.kw = "device_5_8",
		.min = 10
	},
	{
		.kw = "device-5-8",
		.min = 10
	},
	{
		.kw = "device-5_8",
		.min = 10
	},
	{
		.kw = "device_5-8",
		.min = 10
	},
	{
		.kw = "dev5_8",
		.min = 6
	},
	{
		.kw = "dev5-8",
		.min = 6
	},
	{
		.kw = "dev58",
		.min = 5
	},
	{
		.kw = "dev_5_8",
		.min = 7
	},
	{
		.kw = "dev-5-8",
		.min = 7
	},
	{
		.kw = "dev-5_8",
		.min = 7
	},
	{
		.kw = "dev_5-8",
		.min = 7
	},
	{
		.kw = NULL,
		.min = 0
	}
};

static const struct _raster_keyword _raster_document_type_device_6_8_keywords[] = {
	{
		.kw = "Device6_8",
		.min = 9
	},
	{
		.kw = "device6_8",
		.min = 9
	},
	{
		.kw = "device6-8",
		.min = 9
	},
	{
		.kw = "device68",
		.min = 8
	},
	{
		.kw = "device_6_8",
		.min = 10
	},
	{
		.kw = "device-6-8",
		.min = 10
	},
	{
		.kw = "device-6_8",
		.min = 10
	},
	{
		.kw = "device_6-8",
		.min = 10
	},
	{
		.kw = "dev6_8",
		.min = 6
	},
	{
		.kw = "dev6-8",
		.min = 6
	},
	{
		.kw = "dev68",
		.min = 5
	},
	{
		.kw = "dev_6_8",
		.min = 7
	},
	{
		.kw = "dev-6-8",
		.min = 7
	},
	{
		.kw = "dev-6_8",
		.min = 7
	},
	{
		.kw = "dev_6-8",
		.min = 7
	},
	{
		.kw = NULL,
		.min = 0
	}
};

static const struct _raster_keyword _raster_document_type_device_7_8_keywords[] = {
	{
		.kw = "Device7_8",
		.min = 9
	},
	{
		.kw = "device7_8",
		.min = 9
	},
	{
		.kw = "device7-8",
		.min = 9
	},
	{
		.kw = "device78",
		.min = 8
	},
	{
		.kw = "device_7_8",
		.min = 10
	},
	{
		.kw = "device-7-8",
		.min = 10
	},
	{
		.kw = "device-7_8",
		.min = 10
	},
	{
		.kw = "device_7-8",
		.min = 10
	},
	{
		.kw = "dev7_8",
		.min = 6
	},
	{
		.kw = "dev7-8",
		.min = 6
	},
	{
		.kw = "dev78",
		.min = 5
	},
	{
		.kw = "dev_7_8",
		.min = 7
	},
	{
		.kw = "dev-7-8",
		.min = 7
	},
	{
		.kw = "dev-7_8",
		.min = 7
	},
	{
		.kw = "dev_7-8",
		.min = 7
	},
	{
		.kw = NULL,
		.min = 0
	}
};

static const struct _raster_keyword _raster_document_type_device_8_8_keywords[] = {
	{
		.kw = "Device8_8",
		.min = 9
	},
	{
		.kw = "device8_8",
		.min = 9
	},
	{
		.kw = "device8-8",
		.min = 9
	},
	{
		.kw = "device88",
		.min = 8
	},
	{
		.kw = "device_8_8",
		.min = 10
	},
	{
		.kw = "device-8-8",
		.min = 10
	},
	{
		.kw = "device-8_8",
		.min = 10
	},
	{
		.kw = "device_8-8",
		.min = 10
	},
	{
		.kw = "dev8_8",
		.min = 6
	},
	{
		.kw = "dev8-8",
		.min = 6
	},
	{
		.kw = "dev88",
		.min = 5
	},
	{
		.kw = "dev_8_8",
		.min = 7
	},
	{
		.kw = "dev-8-8",
		.min = 7
	},
	{
		.kw = "dev-8_8",
		.min = 7
	},
	{
		.kw = "dev_8-8",
		.min = 7
	},
	{
		.kw = NULL,
		.min = 0
	}
};

static const struct _raster_keyword _raster_document_type_device_9_8_keywords[] = {
	{
		.kw = "Device9_8",
		.min = 9
	},
	{
		.kw = "device9_8",
		.min = 9
	},
	{
		.kw = "device9-8",
		.min = 9
	},
	{
		.kw = "device98",
		.min = 8
	},
	{
		.kw = "device_9_8",
		.min = 10
	},
	{
		.kw = "device-9-8",
		.min = 10
	},
	{
		.kw = "device-9_8",
		.min = 10
	},
	{
		.kw = "device_9-8",
		.min = 10
	},
	{
		.kw = "dev9_8",
		.min = 6
	},
	{
		.kw = "dev9-8",
		.min = 6
	},
	{
		.kw = "dev98",
		.min = 5
	},
	{
		.kw = "dev_9_8",
		.min = 7
	},
	{
		.kw = "dev-9-8",
		.min = 7
	},
	{
		.kw = "dev-9_8",
		.min = 7
	},
	{
		.kw = "dev_9-8",
		.min = 7
	},
	{
		.kw = NULL,
		.min = 0
	}
};

static const struct _raster_keyword _raster_document_type_device_10_8_keywords[] = {
	{
		.kw = "Device10_8",
		.min = 10
	},
	{
		.kw = "device10_8",
		.min = 10
	},
	{
		.kw = "device10-8",
		.min = 10
	},
	{
		.kw = "device108",
		.min = 9
	},
	{
		.kw = "device_10_8",
		.min = 11
	},
	{
		.kw = "device-10-8",
		.min = 11
	},
	{
		.kw = "device-10_8",
		.min = 11
	},
	{
		.kw = "device_10-8",
		.min = 11
	},
	{
		.kw = "dev10_8",
		.min = 7
	},
	{
		.kw = "dev10-8",
		.min = 7
	},
	{
		.kw = "dev108",
		.min = 6
	},
	{
		.kw = "dev_10_8",
		.min = 8
	},
	{
		.kw = "dev-10-8",
		.min = 8
	},
	{
		.kw = "dev-10_8",
		.min = 8
	},
	{
		.kw = "dev_10-8",
		.min = 9
	},
	{
		.kw = "devicea_8",
		.min = 9
	},
	{
		.kw = "devicea-8",
		.min = 9
	},
	{
		.kw = "devicea8",
		.min = 8
	},
	{
		.kw = "device_a_8",
		.min = 10
	},
	{
		.kw = "device-a-8",
		.min = 10
	},
	{
		.kw = "device-a_8",
		.min = 10
	},
	{
		.kw = "device_a-8",
		.min = 10
	},
	{
		.kw = "deva_8",
		.min = 6
	},
	{
		.kw = "deva-8",
		.min = 6
	},
	{
		.kw = "deva8",
		.min = 5
	},
	{
		.kw = "dev_a_8",
		.min = 7
	},
	{
		.kw = "dev-a-8",
		.min = 7
	},
	{
		.kw = "dev-a_8",
		.min = 7
	},
	{
		.kw = "dev_a-8",
		.min = 8
	},
	{
		.kw = NULL,
		.min = 0
	}
};

static const struct _raster_keyword _raster_document_type_device_11_8_keywords[] = {
	{
		.kw = "Device11_8",
		.min = 10
	},
	{
		.kw = "device11_8",
		.min = 10
	},
	{
		.kw = "device11-8",
		.min = 10
	},
	{
		.kw = "device118",
		.min = 9
	},
	{
		.kw = "device_11_8",
		.min = 11
	},
	{
		.kw = "device-11-8",
		.min = 11
	},
	{
		.kw = "device-11_8",
		.min = 11
	},
	{
		.kw = "device_11-8",
		.min = 11
	},
	{
		.kw = "dev11_8",
		.min = 7
	},
	{
		.kw = "dev11-8",
		.min = 7
	},
	{
		.kw = "dev118",
		.min = 6
	},
	{
		.kw = "dev_11_8",
		.min = 8
	},
	{
		.kw = "dev-11-8",
		.min = 8
	},
	{
		.kw = "dev-11_8",
		.min = 8
	},
	{
		.kw = "dev_11-8",
		.min = 9
	},
	{
		.kw = "deviceb_8",
		.min = 9
	},
	{
		.kw = "deviceb-8",
		.min = 9
	},
	{
		.kw = "deviceb8",
		.min = 8
	},
	{
		.kw = "device_b_8",
		.min = 10
	},
	{
		.kw = "device-b-8",
		.min = 10
	},
	{
		.kw = "device-b_8",
		.min = 10
	},
	{
		.kw = "device_b-8",
		.min = 10
	},
	{
		.kw = "devb_8",
		.min = 6
	},
	{
		.kw = "devb-8",
		.min = 6
	},
	{
		.kw = "devb8",
		.min = 5
	},
	{
		.kw = "dev_b_8",
		.min = 7
	},
	{
		.kw = "dev-b-8",
		.min = 7
	},
	{
		.kw = "dev-b_8",
		.min = 7
	},
	{
		.kw = "dev_b-8",
		.min = 8
	},
	{
		.kw = NULL,
		.min = 0
	}
};

static const struct _raster_keyword _raster_document_type_device_12_8_keywords[] = {
	{
		.kw = "Device12_8",
		.min = 10
	},
	{
		.kw = "device12_8",
		.min = 10
	},
	{
		.kw = "device12-8",
		.min = 10
	},
	{
		.kw = "device128",
		.min = 9
	},
	{
		.kw = "device_12_8",
		.min = 11
	},
	{
		.kw = "device-12-8",
		.min = 11
	},
	{
		.kw = "device-12_8",
		.min = 11
	},
	{
		.kw = "device_12-8",
		.min = 11
	},
	{
		.kw = "dev12_8",
		.min = 7
	},
	{
		.kw = "dev12-8",
		.min = 7
	},
	{
		.kw = "dev128",
		.min = 6
	},
	{
		.kw = "dev_12_8",
		.min = 8
	},
	{
		.kw = "dev-12-8",
		.min = 8
	},
	{
		.kw = "dev-12_8",
		.min = 8
	},
	{
		.kw = "dev_12-8",
		.min = 9
	},
	{
		.kw = "devicec_8",
		.min = 9
	},
	{
		.kw = "devicec-8",
		.min = 9
	},
	{
		.kw = "devicec8",
		.min = 8
	},
	{
		.kw = "device_c_8",
		.min = 10
	},
	{
		.kw = "device-c-8",
		.min = 10
	},
	{
		.kw = "device-c_8",
		.min = 10
	},
	{
		.kw = "device_c-8",
		.min = 10
	},
	{
		.kw = "devc_8",
		.min = 6
	},
	{
		.kw = "devc-8",
		.min = 6
	},
	{
		.kw = "devc8",
		.min = 5
	},
	{
		.kw = "dev_c_8",
		.min = 7
	},
	{
		.kw = "dev-c-8",
		.min = 7
	},
	{
		.kw = "dev-c_8",
		.min = 7
	},
	{
		.kw = "dev_c-8",
		.min = 8
	},
	{
		.kw = NULL,
		.min = 0
	}
};

static const struct _raster_keyword _raster_document_type_device_13_8_keywords[] = {
	{
		.kw = "Device13_8",
		.min = 10
	},
	{
		.kw = "device13_8",
		.min = 10
	},
	{
		.kw = "device13-8",
		.min = 10
	},
	{
		.kw = "device138",
		.min = 9
	},
	{
		.kw = "device_13_8",
		.min = 11
	},
	{
		.kw = "device-13-8",
		.min = 11
	},
	{
		.kw = "device-13_8",
		.min = 11
	},
	{
		.kw = "device_13-8",
		.min = 11
	},
	{
		.kw = "dev13_8",
		.min = 7
	},
	{
		.kw = "dev13-8",
		.min = 7
	},
	{
		.kw = "dev138",
		.min = 6
	},
	{
		.kw = "dev_13_8",
		.min = 8
	},
	{
		.kw = "dev-13-8",
		.min = 8
	},
	{
		.kw = "dev-13_8",
		.min = 8
	},
	{
		.kw = "dev_13-8",
		.min = 9
	},
	{
		.kw = "deviced_8",
		.min = 9
	},
	{
		.kw = "deviced-8",
		.min = 9
	},
	{
		.kw = "deviced8",
		.min = 8
	},
	{
		.kw = "device_d_8",
		.min = 10
	},
	{
		.kw = "device-d-8",
		.min = 10
	},
	{
		.kw = "device-d_8",
		.min = 10
	},
	{
		.kw = "device_d-8",
		.min = 10
	},
	{
		.kw = "devd_8",
		.min = 6
	},
	{
		.kw = "devd-8",
		.min = 6
	},
	{
		.kw = "devd8",
		.min = 5
	},
	{
		.kw = "dev_d_8",
		.min = 7
	},
	{
		.kw = "dev-d-8",
		.min = 7
	},
	{
		.kw = "dev-d_8",
		.min = 7
	},
	{
		.kw = "dev_d-8",
		.min = 8
	},
	{
		.kw = NULL,
		.min = 0
	}
};

static const struct _raster_keyword _raster_document_type_device_14_8_keywords[] = {
	{
		.kw = "Device14_8",
		.min = 10
	},
	{
		.kw = "device14_8",
		.min = 10
	},
	{
		.kw = "device14-8",
		.min = 10
	},
	{
		.kw = "device148",
		.min = 9
	},
	{
		.kw = "device_14_8",
		.min = 11
	},
	{
		.kw = "device-14-8",
		.min = 11
	},
	{
		.kw = "device-14_8",
		.min = 11
	},
	{
		.kw = "device_14-8",
		.min = 11
	},
	{
		.kw = "dev14_8",
		.min = 7
	},
	{
		.kw = "dev14-8",
		.min = 7
	},
	{
		.kw = "dev148",
		.min = 6
	},
	{
		.kw = "dev_14_8",
		.min = 8
	},
	{
		.kw = "dev-14-8",
		.min = 8
	},
	{
		.kw = "dev-14_8",
		.min = 8
	},
	{
		.kw = "dev_14-8",
		.min = 9
	},
	{
		.kw = "devicee_8",
		.min = 9
	},
	{
		.kw = "devicee-8",
		.min = 9
	},
	{
		.kw = "devicee8",
		.min = 8
	},
	{
		.kw = "device_e_8",
		.min = 10
	},
	{
		.kw = "device-e-8",
		.min = 10
	},
	{
		.kw = "device-e_8",
		.min = 10
	},
	{
		.kw = "device_e-8",
		.min = 10
	},
	{
		.kw = "deve_8",
		.min = 6
	},
	{
		.kw = "deve-8",
		.min = 6
	},
	{
		.kw = "deve8",
		.min = 5
	},
	{
		.kw = "dev_e_8",
		.min = 7
	},
	{
		.kw = "dev-e-8",
		.min = 7
	},
	{
		.kw = "dev-e_8",
		.min = 7
	},
	{
		.kw = "dev_e-8",
		.min = 8
	},
	{
		.kw = NULL,
		.min = 0
	}
};

static const struct _raster_keyword _raster_document_type_device_15_8_keywords[] = {
	{
		.kw = "Device15_8",
		.min = 10
	},
	{
		.kw = "device15_8",
		.min = 10
	},
	{
		.kw = "device15-8",
		.min = 10
	},
	{
		.kw = "device158",
		.min = 9
	},
	{
		.kw = "device_15_8",
		.min = 11
	},
	{
		.kw = "device-15-8",
		.min = 11
	},
	{
		.kw = "device-15_8",
		.min = 11
	},
	{
		.kw = "device_15-8",
		.min = 11
	},
	{
		.kw = "dev15_8",
		.min = 7
	},
	{
		.kw = "dev15-8",
		.min = 7
	},
	{
		.kw = "dev158",
		.min = 6
	},
	{
		.kw = "dev_15_8",
		.min = 8
	},
	{
		.kw = "dev-15-8",
		.min = 8
	},
	{
		.kw = "dev-15_8",
		.min = 8
	},
	{
		.kw = "dev_15-8",
		.min = 9
	},
	{
		.kw = "devicef_8",
		.min = 9
	},
	{
		.kw = "devicef-8",
		.min = 9
	},
	{
		.kw = "devicef8",
		.min = 8
	},
	{
		.kw = "device_f_8",
		.min = 10
	},
	{
		.kw = "device-f-8",
		.min = 10
	},
	{
		.kw = "device-f_8",
		.min = 10
	},
	{
		.kw = "device_f-8",
		.min = 10
	},
	{
		.kw = "devf_8",
		.min = 6
	},
	{
		.kw = "devf-8",
		.min = 6
	},
	{
		.kw = "devf8",
		.min = 5
	},
	{
		.kw = "dev_f_8",
		.min = 7
	},
	{
		.kw = "dev-f-8",
		.min = 7
	},
	{
		.kw = "dev-f_8",
		.min = 7
	},
	{
		.kw = "dev_f-8",
		.min = 8
	},
	{
		.kw = NULL,
		.min = 0
	}
};

static const struct _raster_keyword _raster_document_type_rgb_8_keywords[] =
{
	{
		.kw = "Rgb_8",
		.min = 5
	},
	{
		.kw = "rgb_8",
		.min = 5
	},
	{
		.kw = "rgb8",
		.min = 4
	},
	{
		.kw = NULL,
		.min = 0
	}
};

static const struct _raster_keyword _raster_document_type_sgray_8_keywords[] =
{
	{
		.kw = "Sgray_8",
		.min = 7
	},
	{
		.kw = "sgray_8",
		.min = 7
	},
	{
		.kw = "sgray8",
		.min = 6
	},
	{
		.kw = NULL,
		.min = 0
	}
};

static const struct _raster_keyword _raster_document_type_srgb_8_keywords[] =
{
	{
		.kw = "Srgb_8",
		.min = 6
	},
	{
		.kw = "srgb_8",
		.min = 6
	},
	{
		.kw = "srgb8",
		.min = 5
	},
	{
		.kw = NULL,
		.min = 0
	}
};

static const struct _raster_keyword _raster_document_type_adobe_rgb_16_keywords[] =
{
	{
		.kw = "AdobeRgb_16",
		.min = 12
	},
	{
		.kw = "adobe-rgb_16",
		.min = 12
	},
	{
		.kw = "adobe-rgb16",
		.min = 11
	},
	{
		.kw = NULL,
		.min = 0
	}
};

static const struct _raster_keyword _raster_document_type_black_16_keywords[] =
{
	{
		.kw = "Black_16",
		.min = 8
	},
	{
		.kw = "black_16",
		.min = 8
	},
	{
		.kw = "black16",
		.min = 7
	},
	{
		.kw = NULL,
		.min = 0
	}
};

static const struct _raster_keyword _raster_document_type_cmyk_16_keywords[] =
{
	{
		.kw = "Cmyk_16",
		.min = 7
	},
	{
		.kw = "cmyk_16",
		.min = 7
	},
	{
		.kw = "cmyk16",
		.min = 6
	},
	{
		.kw = NULL,
		.min = 0
	}
};


static const struct _raster_keyword _raster_document_type_device_1_16_keywords[] = {
	{
		.kw = "Device1_16",
		.min = 9
	},
	{
		.kw = "device1_16",
		.min = 9
	},
	{
		.kw = "device1-16",
		.min = 9
	},
	{
		.kw = "device116",
		.min = 8
	},
	{
		.kw = "device_1_16",
		.min = 10
	},
	{
		.kw = "device-1-16",
		.min = 10
	},
	{
		.kw = "device-1_16",
		.min = 10
	},
	{
		.kw = "device_1-16",
		.min = 10
	},
	{
		.kw = "dev1_16",
		.min = 6
	},
	{
		.kw = "dev1-16",
		.min = 6
	},
	{
		.kw = "dev116",
		.min = 5
	},
	{
		.kw = "dev_1_16",
		.min = 7
	},
	{
		.kw = "dev-1-16",
		.min = 7
	},
	{
		.kw = "dev-1_16",
		.min = 7
	},
	{
		.kw = "dev_1-16",
		.min = 7
	},
	{
		.kw = NULL,
		.min = 0
	}
};

static const struct _raster_keyword _raster_document_type_device_2_16_keywords[] = {
	{
		.kw = "Device2_16",
		.min = 9
	},
	{
		.kw = "device2_16",
		.min = 9
	},
	{
		.kw = "device2-16",
		.min = 9
	},
	{
		.kw = "device216",
		.min = 8
	},
	{
		.kw = "device_2_16",
		.min = 10
	},
	{
		.kw = "device-2-16",
		.min = 10
	},
	{
		.kw = "device-2_16",
		.min = 10
	},
	{
		.kw = "device_2-16",
		.min = 10
	},
	{
		.kw = "dev2_16",
		.min = 6
	},
	{
		.kw = "dev2-16",
		.min = 6
	},
	{
		.kw = "dev216",
		.min = 5
	},
	{
		.kw = "dev_2_16",
		.min = 7
	},
	{
		.kw = "dev-2-16",
		.min = 7
	},
	{
		.kw = "dev-2_16",
		.min = 7
	},
	{
		.kw = "dev_2-16",
		.min = 7
	},
	{
		.kw = NULL,
		.min = 0
	}
};

static const struct _raster_keyword _raster_document_type_device_3_16_keywords[] = {
	{
		.kw = "Device3_16",
		.min = 9
	},
	{
		.kw = "device3_16",
		.min = 9
	},
	{
		.kw = "device3-16",
		.min = 9
	},
	{
		.kw = "device316",
		.min = 8
	},
	{
		.kw = "device_3_16",
		.min = 10
	},
	{
		.kw = "device-3-16",
		.min = 10
	},
	{
		.kw = "device-3_16",
		.min = 10
	},
	{
		.kw = "device_3-16",
		.min = 10
	},
	{
		.kw = "dev3_16",
		.min = 6
	},
	{
		.kw = "dev3-16",
		.min = 6
	},
	{
		.kw = "dev316",
		.min = 5
	},
	{
		.kw = "dev_3_16",
		.min = 7
	},
	{
		.kw = "dev-3-16",
		.min = 7
	},
	{
		.kw = "dev-3_16",
		.min = 7
	},
	{
		.kw = "dev_3-16",
		.min = 7
	},
	{
		.kw = NULL,
		.min = 0
	}
};

static const struct _raster_keyword _raster_document_type_device_4_16_keywords[] = {
	{
		.kw = "Device4_16",
		.min = 9
	},
	{
		.kw = "device4_16",
		.min = 9
	},
	{
		.kw = "device4-16",
		.min = 9
	},
	{
		.kw = "device416",
		.min = 8
	},
	{
		.kw = "device_4_16",
		.min = 10
	},
	{
		.kw = "device-4-16",
		.min = 10
	},
	{
		.kw = "device-4_16",
		.min = 10
	},
	{
		.kw = "device_4-16",
		.min = 10
	},
	{
		.kw = "dev4_16",
		.min = 6
	},
	{
		.kw = "dev4-16",
		.min = 6
	},
	{
		.kw = "dev416",
		.min = 5
	},
	{
		.kw = "dev_4_16",
		.min = 7
	},
	{
		.kw = "dev-4-16",
		.min = 7
	},
	{
		.kw = "dev-4_16",
		.min = 7
	},
	{
		.kw = "dev_4-16",
		.min = 7
	},
	{
		.kw = NULL,
		.min = 0
	}
};

static const struct _raster_keyword _raster_document_type_device_5_16_keywords[] = {
	{
		.kw = "Device5_16",
		.min = 9
	},
	{
		.kw = "device5_16",
		.min = 9
	},
	{
		.kw = "device5-16",
		.min = 9
	},
	{
		.kw = "device516",
		.min = 8
	},
	{
		.kw = "device_5_16",
		.min = 10
	},
	{
		.kw = "device-5-16",
		.min = 10
	},
	{
		.kw = "device-5_16",
		.min = 10
	},
	{
		.kw = "device_5-16",
		.min = 10
	},
	{
		.kw = "dev5_16",
		.min = 6
	},
	{
		.kw = "dev5-16",
		.min = 6
	},
	{
		.kw = "dev516",
		.min = 5
	},
	{
		.kw = "dev_5_16",
		.min = 7
	},
	{
		.kw = "dev-5-16",
		.min = 7
	},
	{
		.kw = "dev-5_16",
		.min = 7
	},
	{
		.kw = "dev_5-16",
		.min = 7
	},
	{
		.kw = NULL,
		.min = 0
	}
};

static const struct _raster_keyword _raster_document_type_device_6_16_keywords[] = {
	{
		.kw = "Device6_16",
		.min = 9
	},
	{
		.kw = "device6_16",
		.min = 9
	},
	{
		.kw = "device6-16",
		.min = 9
	},
	{
		.kw = "device616",
		.min = 8
	},
	{
		.kw = "device_6_16",
		.min = 10
	},
	{
		.kw = "device-6-16",
		.min = 10
	},
	{
		.kw = "device-6_16",
		.min = 10
	},
	{
		.kw = "device_6-16",
		.min = 10
	},
	{
		.kw = "dev6_16",
		.min = 6
	},
	{
		.kw = "dev6-16",
		.min = 6
	},
	{
		.kw = "dev616",
		.min = 5
	},
	{
		.kw = "dev_6_16",
		.min = 7
	},
	{
		.kw = "dev-6-16",
		.min = 7
	},
	{
		.kw = "dev-6_16",
		.min = 7
	},
	{
		.kw = "dev_6-16",
		.min = 7
	},
	{
		.kw = NULL,
		.min = 0
	}
};

static const struct _raster_keyword _raster_document_type_device_7_16_keywords[] = {
	{
		.kw = "Device7_16",
		.min = 9
	},
	{
		.kw = "device7_16",
		.min = 9
	},
	{
		.kw = "device7-16",
		.min = 9
	},
	{
		.kw = "device716",
		.min = 8
	},
	{
		.kw = "device_7_16",
		.min = 10
	},
	{
		.kw = "device-7-16",
		.min = 10
	},
	{
		.kw = "device-7_16",
		.min = 10
	},
	{
		.kw = "device_7-16",
		.min = 10
	},
	{
		.kw = "dev7_16",
		.min = 6
	},
	{
		.kw = "dev7-16",
		.min = 6
	},
	{
		.kw = "dev716",
		.min = 5
	},
	{
		.kw = "dev_7_16",
		.min = 7
	},
	{
		.kw = "dev-7-16",
		.min = 7
	},
	{
		.kw = "dev-7_16",
		.min = 7
	},
	{
		.kw = "dev_7-16",
		.min = 7
	},
	{
		.kw = NULL,
		.min = 0
	}
};

static const struct _raster_keyword _raster_document_type_device_8_16_keywords[] = {
	{
		.kw = "Device8_16",
		.min = 9
	},
	{
		.kw = "device8_16",
		.min = 9
	},
	{
		.kw = "device8-16",
		.min = 9
	},
	{
		.kw = "device816",
		.min = 8
	},
	{
		.kw = "device_8_16",
		.min = 10
	},
	{
		.kw = "device-8-16",
		.min = 10
	},
	{
		.kw = "device-8_16",
		.min = 10
	},
	{
		.kw = "device_8-16",
		.min = 10
	},
	{
		.kw = "dev8_16",
		.min = 6
	},
	{
		.kw = "dev8-16",
		.min = 6
	},
	{
		.kw = "dev816",
		.min = 5
	},
	{
		.kw = "dev_8_16",
		.min = 7
	},
	{
		.kw = "dev-8-16",
		.min = 7
	},
	{
		.kw = "dev-8_16",
		.min = 7
	},
	{
		.kw = "dev_8-16",
		.min = 7
	},
	{
		.kw = NULL,
		.min = 0
	}
};

static const struct _raster_keyword _raster_document_type_device_9_16_keywords[] = {
	{
		.kw = "Device9_16",
		.min = 9
	},
	{
		.kw = "device9_16",
		.min = 9
	},
	{
		.kw = "device9-16",
		.min = 9
	},
	{
		.kw = "device916",
		.min = 8
	},
	{
		.kw = "device_9_16",
		.min = 10
	},
	{
		.kw = "device-9-16",
		.min = 10
	},
	{
		.kw = "device-9_16",
		.min = 10
	},
	{
		.kw = "device_9-16",
		.min = 10
	},
	{
		.kw = "dev9_16",
		.min = 6
	},
	{
		.kw = "dev9-16",
		.min = 6
	},
	{
		.kw = "dev916",
		.min = 5
	},
	{
		.kw = "dev_9_16",
		.min = 7
	},
	{
		.kw = "dev-9-16",
		.min = 7
	},
	{
		.kw = "dev-9_16",
		.min = 7
	},
	{
		.kw = "dev_9-16",
		.min = 7
	},
	{
		.kw = NULL,
		.min = 0
	}
};

static const struct _raster_keyword _raster_document_type_device_10_16_keywords[] = {
	{
		.kw = "Device10_16",
		.min = 10
	},
	{
		.kw = "device10_16",
		.min = 10
	},
	{
		.kw = "device10-16",
		.min = 10
	},
	{
		.kw = "device1016",
		.min = 9
	},
	{
		.kw = "device_10_16",
		.min = 11
	},
	{
		.kw = "device-10-16",
		.min = 11
	},
	{
		.kw = "device-10_16",
		.min = 11
	},
	{
		.kw = "device_10-16",
		.min = 11
	},
	{
		.kw = "dev10_16",
		.min = 7
	},
	{
		.kw = "dev10-16",
		.min = 7
	},
	{
		.kw = "dev1016",
		.min = 6
	},
	{
		.kw = "dev_10_16",
		.min = 8
	},
	{
		.kw = "dev-10-16",
		.min = 8
	},
	{
		.kw = "dev-10_16",
		.min = 8
	},
	{
		.kw = "dev_10-16",
		.min = 9
	},
	{
		.kw = "devicea_16",
		.min = 9
	},
	{
		.kw = "devicea-16",
		.min = 9
	},
	{
		.kw = "devicea16",
		.min = 8
	},
	{
		.kw = "device_a_16",
		.min = 10
	},
	{
		.kw = "device-a-16",
		.min = 10
	},
	{
		.kw = "device-a_16",
		.min = 10
	},
	{
		.kw = "device_a-16",
		.min = 10
	},
	{
		.kw = "deva_16",
		.min = 6
	},
	{
		.kw = "deva-16",
		.min = 6
	},
	{
		.kw = "deva16",
		.min = 5
	},
	{
		.kw = "dev_a_16",
		.min = 7
	},
	{
		.kw = "dev-a-16",
		.min = 7
	},
	{
		.kw = "dev-a_16",
		.min = 7
	},
	{
		.kw = "dev_a-16",
		.min = 8
	},
	{
		.kw = NULL,
		.min = 0
	}
};

static const struct _raster_keyword _raster_document_type_device_11_16_keywords[] = {
	{
		.kw = "Device11_16",
		.min = 10
	},
	{
		.kw = "device11_16",
		.min = 10
	},
	{
		.kw = "device11-16",
		.min = 10
	},
	{
		.kw = "device1116",
		.min = 9
	},
	{
		.kw = "device_11_16",
		.min = 11
	},
	{
		.kw = "device-11-16",
		.min = 11
	},
	{
		.kw = "device-11_16",
		.min = 11
	},
	{
		.kw = "device_11-16",
		.min = 11
	},
	{
		.kw = "dev11_16",
		.min = 7
	},
	{
		.kw = "dev11-16",
		.min = 7
	},
	{
		.kw = "dev1116",
		.min = 6
	},
	{
		.kw = "dev_11_16",
		.min = 8
	},
	{
		.kw = "dev-11-16",
		.min = 8
	},
	{
		.kw = "dev-11_16",
		.min = 8
	},
	{
		.kw = "dev_11-16",
		.min = 9
	},
	{
		.kw = "deviceb_16",
		.min = 9
	},
	{
		.kw = "deviceb-16",
		.min = 9
	},
	{
		.kw = "deviceb16",
		.min = 8
	},
	{
		.kw = "device_b_16",
		.min = 10
	},
	{
		.kw = "device-b-16",
		.min = 10
	},
	{
		.kw = "device-b_16",
		.min = 10
	},
	{
		.kw = "device_b-16",
		.min = 10
	},
	{
		.kw = "devb_16",
		.min = 6
	},
	{
		.kw = "devb-16",
		.min = 6
	},
	{
		.kw = "devb16",
		.min = 5
	},
	{
		.kw = "dev_b_16",
		.min = 7
	},
	{
		.kw = "dev-b-16",
		.min = 7
	},
	{
		.kw = "dev-b_16",
		.min = 7
	},
	{
		.kw = "dev_b-16",
		.min = 8
	},
	{
		.kw = NULL,
		.min = 0
	}
};

static const struct _raster_keyword _raster_document_type_device_12_16_keywords[] = {
	{
		.kw = "Device12_16",
		.min = 10
	},
	{
		.kw = "device12_16",
		.min = 10
	},
	{
		.kw = "device12-16",
		.min = 10
	},
	{
		.kw = "device1216",
		.min = 9
	},
	{
		.kw = "device_12_16",
		.min = 11
	},
	{
		.kw = "device-12-16",
		.min = 11
	},
	{
		.kw = "device-12_16",
		.min = 11
	},
	{
		.kw = "device_12-16",
		.min = 11
	},
	{
		.kw = "dev12_16",
		.min = 7
	},
	{
		.kw = "dev12-16",
		.min = 7
	},
	{
		.kw = "dev1216",
		.min = 6
	},
	{
		.kw = "dev_12_16",
		.min = 8
	},
	{
		.kw = "dev-12-16",
		.min = 8
	},
	{
		.kw = "dev-12_16",
		.min = 8
	},
	{
		.kw = "dev_12-16",
		.min = 9
	},
	{
		.kw = "devicec_16",
		.min = 9
	},
	{
		.kw = "devicec-16",
		.min = 9
	},
	{
		.kw = "devicec16",
		.min = 8
	},
	{
		.kw = "device_c_16",
		.min = 10
	},
	{
		.kw = "device-c-16",
		.min = 10
	},
	{
		.kw = "device-c_16",
		.min = 10
	},
	{
		.kw = "device_c-16",
		.min = 10
	},
	{
		.kw = "devc_16",
		.min = 6
	},
	{
		.kw = "devc-16",
		.min = 6
	},
	{
		.kw = "devc16",
		.min = 5
	},
	{
		.kw = "dev_c_16",
		.min = 7
	},
	{
		.kw = "dev-c-16",
		.min = 7
	},
	{
		.kw = "dev-c_16",
		.min = 7
	},
	{
		.kw = "dev_c-16",
		.min = 8
	},
	{
		.kw = NULL,
		.min = 0
	}
};

static const struct _raster_keyword _raster_document_type_device_13_16_keywords[] = {
	{
		.kw = "Device13_16",
		.min = 10
	},
	{
		.kw = "device13_16",
		.min = 10
	},
	{
		.kw = "device13-16",
		.min = 10
	},
	{
		.kw = "device1316",
		.min = 9
	},
	{
		.kw = "device_13_16",
		.min = 11
	},
	{
		.kw = "device-13-16",
		.min = 11
	},
	{
		.kw = "device-13_16",
		.min = 11
	},
	{
		.kw = "device_13-16",
		.min = 11
	},
	{
		.kw = "dev13_16",
		.min = 7
	},
	{
		.kw = "dev13-16",
		.min = 7
	},
	{
		.kw = "dev1316",
		.min = 6
	},
	{
		.kw = "dev_13_16",
		.min = 8
	},
	{
		.kw = "dev-13-16",
		.min = 8
	},
	{
		.kw = "dev-13_16",
		.min = 8
	},
	{
		.kw = "dev_13-16",
		.min = 9
	},
	{
		.kw = "deviced_16",
		.min = 9
	},
	{
		.kw = "deviced-16",
		.min = 9
	},
	{
		.kw = "deviced16",
		.min = 8
	},
	{
		.kw = "device_d_16",
		.min = 10
	},
	{
		.kw = "device-d-16",
		.min = 10
	},
	{
		.kw = "device-d_16",
		.min = 10
	},
	{
		.kw = "device_d-16",
		.min = 10
	},
	{
		.kw = "devd_16",
		.min = 6
	},
	{
		.kw = "devd-16",
		.min = 6
	},
	{
		.kw = "devd16",
		.min = 5
	},
	{
		.kw = "dev_d_16",
		.min = 7
	},
	{
		.kw = "dev-d-16",
		.min = 7
	},
	{
		.kw = "dev-d_16",
		.min = 7
	},
	{
		.kw = "dev_d-16",
		.min = 8
	},
	{
		.kw = NULL,
		.min = 0
	}
};

static const struct _raster_keyword _raster_document_type_device_14_16_keywords[] = {
	{
		.kw = "Device14_16",
		.min = 10
	},
	{
		.kw = "device14_16",
		.min = 10
	},
	{
		.kw = "device14-16",
		.min = 10
	},
	{
		.kw = "device1416",
		.min = 9
	},
	{
		.kw = "device_14_16",
		.min = 11
	},
	{
		.kw = "device-14-16",
		.min = 11
	},
	{
		.kw = "device-14_16",
		.min = 11
	},
	{
		.kw = "device_14-16",
		.min = 11
	},
	{
		.kw = "dev14_16",
		.min = 7
	},
	{
		.kw = "dev14-16",
		.min = 7
	},
	{
		.kw = "dev1416",
		.min = 6
	},
	{
		.kw = "dev_14_16",
		.min = 8
	},
	{
		.kw = "dev-14-16",
		.min = 8
	},
	{
		.kw = "dev-14_16",
		.min = 8
	},
	{
		.kw = "dev_14-16",
		.min = 9
	},
	{
		.kw = "devicee_16",
		.min = 9
	},
	{
		.kw = "devicee-16",
		.min = 9
	},
	{
		.kw = "devicee16",
		.min = 8
	},
	{
		.kw = "device_e_16",
		.min = 10
	},
	{
		.kw = "device-e-16",
		.min = 10
	},
	{
		.kw = "device-e_16",
		.min = 10
	},
	{
		.kw = "device_e-16",
		.min = 10
	},
	{
		.kw = "deve_16",
		.min = 6
	},
	{
		.kw = "deve-16",
		.min = 6
	},
	{
		.kw = "deve16",
		.min = 5
	},
	{
		.kw = "dev_e_16",
		.min = 7
	},
	{
		.kw = "dev-e-16",
		.min = 7
	},
	{
		.kw = "dev-e_16",
		.min = 7
	},
	{
		.kw = "dev_e-16",
		.min = 8
	},
	{
		.kw = NULL,
		.min = 0
	}
};

static const struct _raster_keyword _raster_document_type_device_15_16_keywords[] = {
	{
		.kw = "Device15_16",
		.min = 10
	},
	{
		.kw = "device15_16",
		.min = 10
	},
	{
		.kw = "device15-16",
		.min = 10
	},
	{
		.kw = "device1516",
		.min = 9
	},
	{
		.kw = "device_15_16",
		.min = 11
	},
	{
		.kw = "device-15-16",
		.min = 11
	},
	{
		.kw = "device-15_16",
		.min = 11
	},
	{
		.kw = "device_15-16",
		.min = 11
	},
	{
		.kw = "dev15_16",
		.min = 7
	},
	{
		.kw = "dev15-16",
		.min = 7
	},
	{
		.kw = "dev1516",
		.min = 6
	},
	{
		.kw = "dev_15_16",
		.min = 8
	},
	{
		.kw = "dev-15-16",
		.min = 8
	},
	{
		.kw = "dev-15_16",
		.min = 8
	},
	{
		.kw = "dev_15-16",
		.min = 9
	},
	{
		.kw = "devicef_16",
		.min = 9
	},
	{
		.kw = "devicef-16",
		.min = 9
	},
	{
		.kw = "devicef16",
		.min = 8
	},
	{
		.kw = "device_f_16",
		.min = 10
	},
	{
		.kw = "device-f-16",
		.min = 10
	},
	{
		.kw = "device-f_16",
		.min = 10
	},
	{
		.kw = "device_f-16",
		.min = 10
	},
	{
		.kw = "devf_16",
		.min = 6
	},
	{
		.kw = "devf-16",
		.min = 6
	},
	{
		.kw = "devf16",
		.min = 5
	},
	{
		.kw = "dev_f_16",
		.min = 7
	},
	{
		.kw = "dev-f-16",
		.min = 7
	},
	{
		.kw = "dev-f_16",
		.min = 7
	},
	{
		.kw = "dev_f-16",
		.min = 8
	},
	{
		.kw = NULL,
		.min = 0
	}
};

static const struct _raster_keyword _raster_document_type_rgb_16_keywords[] =
{
	{
		.kw = "Rgb_16",
		.min = 6
	},
	{
		.kw = "rgb_16",
		.min = 6
	},
	{
		.kw = "rgb16",
		.min = 5
	},
	{
		.kw = NULL,
		.min = 0
	}
};

static const struct _raster_keyword _raster_document_type_sgray_16_keywords[] =
{
	{
		.kw = "Sgray_16",
		.min = 8
	},
	{
		.kw = "sgray_16",
		.min = 8
	},
	{
		.kw = "sgray16",
		.min = 7
	},
	{
		.kw = NULL,
		.min = 0
	}
};

static const struct _raster_keyword _raster_document_type_srgb_16_keywords[] =
{
	{
		.kw = "Srgb_16",
		.min = 7
	},
	{
		.kw = "srgb_16",
		.min = 7
	},
	{
		.kw = "srgb16",
		.min = 6
	},
	{
		.kw = NULL,
		.min = 0
	}
};

static const struct _raster_document_type _raster_document_type[] = 
{
	{ .document_type = RASTER_DOCUMENT_TYPE_BLACK_1,
	  .keywords = _raster_document_type_black_1_keywords,
	  .color_space = RASTER_COLOR_SPACE_K,
	  .bits_per_pixel = 1,
	  .bits_per_color = 1,
	  .num_colors = 1
	},
	{ .document_type = RASTER_DOCUMENT_TYPE_SGRAY_1,
	  .keywords = _raster_document_type_sgray_1_keywords,
	  .color_space = RASTER_COLOR_SPACE_SW,
	  .bits_per_pixel = 1,
	  .bits_per_color = 1,
	  .num_colors = 1
	},
	{ .document_type = RASTER_DOCUMENT_TYPE_ADOBE_RGB_8,
	  .keywords = _raster_document_type_adobe_rgb_8_keywords,
	  .color_space = RASTER_COLOR_SPACE_ADOBERGB,
	  .bits_per_pixel = 8,
	  .bits_per_color = 24,
	  .num_colors = 3
	},
	{ .document_type = RASTER_DOCUMENT_TYPE_BLACK_8,
	  .keywords = _raster_document_type_black_8_keywords,
	  .color_space = RASTER_COLOR_SPACE_K,
	  .bits_per_pixel = 8,
	  .bits_per_color = 8,
	  .num_colors = 1
	},
	{ .document_type = RASTER_DOCUMENT_TYPE_CMYK_8,
	  .keywords = _raster_document_type_cmyk_8_keywords,
	  .color_space = RASTER_COLOR_SPACE_CMYK,
	  .bits_per_pixel = 8,
	  .bits_per_color = 32,
	  .num_colors = 4
	},
	{ .document_type = RASTER_DOCUMENT_TYPE_DEVICE_1_8,
	  .keywords = _raster_document_type_device_1_8_keywords,
	  .color_space = RASTER_COLOR_SPACE_DEVICE_1,
	  .bits_per_pixel = 8,
	  .bits_per_color = 8,
	  .num_colors = 1
	},
	{ .document_type = RASTER_DOCUMENT_TYPE_DEVICE_2_8,
	  .keywords = _raster_document_type_device_2_8_keywords,
	  .color_space = RASTER_COLOR_SPACE_DEVICE_2,
	  .bits_per_pixel = 8,
	  .bits_per_color = 16,
	  .num_colors = 2
	},
	{ .document_type = RASTER_DOCUMENT_TYPE_DEVICE_3_8,
	  .keywords = _raster_document_type_device_3_8_keywords,
	  .color_space = RASTER_COLOR_SPACE_DEVICE_3,
	  .bits_per_pixel = 8,
	  .bits_per_color = 24,
	  .num_colors = 3
	},
	{ .document_type = RASTER_DOCUMENT_TYPE_DEVICE_4_8,
	  .keywords = _raster_document_type_device_4_8_keywords,
	  .color_space = RASTER_COLOR_SPACE_DEVICE_4,
	  .bits_per_pixel = 8,
	  .bits_per_color = 32,
	  .num_colors = 4
	},
	{ .document_type = RASTER_DOCUMENT_TYPE_DEVICE_5_8,
	  .keywords = _raster_document_type_device_5_8_keywords,
	  .color_space = RASTER_COLOR_SPACE_DEVICE_5,
	  .bits_per_pixel = 8,
	  .bits_per_color = 40,
	  .num_colors = 5
	},
	{ .document_type = RASTER_DOCUMENT_TYPE_DEVICE_6_8,
	  .keywords = _raster_document_type_device_6_8_keywords,
	  .color_space = RASTER_COLOR_SPACE_DEVICE_6,
	  .bits_per_pixel = 8,
	  .bits_per_color = 48,
	  .num_colors = 6
	},
	{ .document_type = RASTER_DOCUMENT_TYPE_DEVICE_7_8,
	  .keywords = _raster_document_type_device_7_8_keywords,
	  .color_space = RASTER_COLOR_SPACE_DEVICE_7,
	  .bits_per_pixel = 8,
	  .bits_per_color = 56,
	  .num_colors = 7
	},
	{ .document_type = RASTER_DOCUMENT_TYPE_DEVICE_8_8,
	  .keywords = _raster_document_type_device_8_8_keywords,
	  .color_space = RASTER_COLOR_SPACE_DEVICE_8,
	  .bits_per_pixel = 8,
	  .bits_per_color = 64,
	  .num_colors = 8
	},
	{ .document_type = RASTER_DOCUMENT_TYPE_DEVICE_9_8,
	  .keywords = _raster_document_type_device_9_8_keywords,
	  .color_space = RASTER_COLOR_SPACE_DEVICE_9,
	  .bits_per_pixel = 8,
	  .bits_per_color = 72,
	  .num_colors = 9
	},
	{ .document_type = RASTER_DOCUMENT_TYPE_DEVICE_10_8,
	  .keywords = _raster_document_type_device_10_8_keywords,
	  .color_space = RASTER_COLOR_SPACE_DEVICE_10,
	  .bits_per_pixel = 8,
	  .bits_per_color = 80,
	  .num_colors = 10
	},
	{ .document_type = RASTER_DOCUMENT_TYPE_DEVICE_11_8,
	  .keywords = _raster_document_type_device_11_8_keywords,
	  .color_space = RASTER_COLOR_SPACE_DEVICE_11,
	  .bits_per_pixel = 8,
	  .bits_per_color = 88,
	  .num_colors = 11
	},
	{ .document_type = RASTER_DOCUMENT_TYPE_DEVICE_12_8,
	  .keywords = _raster_document_type_device_12_8_keywords,
	  .color_space = RASTER_COLOR_SPACE_DEVICE_12,
	  .bits_per_pixel = 8,
	  .bits_per_color = 96,
	  .num_colors = 12
	},
	{ .document_type = RASTER_DOCUMENT_TYPE_DEVICE_13_8,
	  .keywords = _raster_document_type_device_13_8_keywords,
	  .color_space = RASTER_COLOR_SPACE_DEVICE_13,
	  .bits_per_pixel = 8,
	  .bits_per_color = 104,
	  .num_colors = 13
	},
	{ .document_type = RASTER_DOCUMENT_TYPE_DEVICE_14_8,
	  .keywords = _raster_document_type_device_14_8_keywords,
	  .color_space = RASTER_COLOR_SPACE_DEVICE_14,
	  .bits_per_pixel = 8,
	  .bits_per_color = 112,
	  .num_colors = 14
	},
	{ .document_type = RASTER_DOCUMENT_TYPE_DEVICE_15_8,
	  .keywords = _raster_document_type_device_15_8_keywords,
	  .color_space = RASTER_COLOR_SPACE_DEVICE_15,
	  .bits_per_pixel = 8,
	  .bits_per_color = 120,
	  .num_colors = 15
	},
	{ .document_type = RASTER_DOCUMENT_TYPE_RGB_8,
	  .keywords = _raster_document_type_rgb_8_keywords,
	  .color_space = RASTER_COLOR_SPACE_RGB,
	  .bits_per_pixel = 8,
	  .bits_per_color = 24,
	  .num_colors = 3
	},
	{ .document_type = RASTER_DOCUMENT_TYPE_SGRAY_8,
	  .keywords = _raster_document_type_sgray_8_keywords,
	  .color_space = RASTER_COLOR_SPACE_SW,
	  .bits_per_pixel = 8,
	  .bits_per_color = 8,
	  .num_colors = 1
	},
	{ .document_type = RASTER_DOCUMENT_TYPE_SRGB_8,
	  .keywords = _raster_document_type_srgb_8_keywords,
	  .color_space = RASTER_COLOR_SPACE_SRGB,
	  .bits_per_pixel = 8,
	  .bits_per_color = 24,
	  .num_colors = 3
	},
	{ .document_type = RASTER_DOCUMENT_TYPE_ADOBE_RGB_16,
	  .keywords = _raster_document_type_adobe_rgb_16_keywords,
	  .color_space = RASTER_COLOR_SPACE_ADOBERGB,
	  .bits_per_pixel = 16,
	  .bits_per_color = 48,
	  .num_colors = 3
	},
	{ .document_type = RASTER_DOCUMENT_TYPE_BLACK_16,
	  .keywords = _raster_document_type_black_16_keywords,
	  .color_space = RASTER_COLOR_SPACE_K,
	  .bits_per_pixel = 16,
	  .bits_per_color = 16,
	  .num_colors = 1
	},
	{ .document_type = RASTER_DOCUMENT_TYPE_CMYK_16,
	  .keywords = _raster_document_type_cmyk_16_keywords,
	  .color_space = RASTER_COLOR_SPACE_CMYK,
	  .bits_per_pixel = 16,
	  .bits_per_color = 64,
	  .num_colors = 4
	},
	{ .document_type = RASTER_DOCUMENT_TYPE_DEVICE_1_16,
	  .keywords = _raster_document_type_device_1_16_keywords,
	  .color_space = RASTER_COLOR_SPACE_DEVICE_1,
	  .bits_per_pixel = 16,
	  .bits_per_color = 16,
	  .num_colors = 1
	},
	{ .document_type = RASTER_DOCUMENT_TYPE_DEVICE_2_16,
	  .keywords = _raster_document_type_device_2_16_keywords,
	  .color_space = RASTER_COLOR_SPACE_DEVICE_2,
	  .bits_per_pixel = 16,
	  .bits_per_color = 32,
	  .num_colors = 2
	},
	{ .document_type = RASTER_DOCUMENT_TYPE_DEVICE_3_16,
	  .keywords = _raster_document_type_device_3_16_keywords,
	  .color_space = RASTER_COLOR_SPACE_DEVICE_3,
	  .bits_per_pixel = 16,
	  .bits_per_color = 48,
	  .num_colors = 3
	},
	{ .document_type = RASTER_DOCUMENT_TYPE_DEVICE_4_16,
	  .keywords = _raster_document_type_device_4_16_keywords,
	  .color_space = RASTER_COLOR_SPACE_DEVICE_4,
	  .bits_per_pixel = 16,
	  .bits_per_color = 64,
	  .num_colors = 4
	},
	{ .document_type = RASTER_DOCUMENT_TYPE_DEVICE_5_16,
	  .keywords = _raster_document_type_device_5_16_keywords,
	  .color_space = RASTER_COLOR_SPACE_DEVICE_5,
	  .bits_per_pixel = 16,
	  .bits_per_color = 80,
	  .num_colors = 5
	},
	{ .document_type = RASTER_DOCUMENT_TYPE_DEVICE_6_16,
	  .keywords = _raster_document_type_device_6_16_keywords,
	  .color_space = RASTER_COLOR_SPACE_DEVICE_6,
	  .bits_per_pixel = 16,
	  .bits_per_color = 96,
	  .num_colors = 6
	},
	{ .document_type = RASTER_DOCUMENT_TYPE_DEVICE_7_16,
	  .keywords = _raster_document_type_device_7_16_keywords,
	  .color_space = RASTER_COLOR_SPACE_DEVICE_7,
	  .bits_per_pixel = 16,
	  .bits_per_color = 112,
	  .num_colors = 7
	},
	{ .document_type = RASTER_DOCUMENT_TYPE_DEVICE_8_16,
	  .keywords = _raster_document_type_device_8_16_keywords,
	  .color_space = RASTER_COLOR_SPACE_DEVICE_8,
	  .bits_per_pixel = 16,
	  .bits_per_color = 128,
	  .num_colors = 8
	},
	{ .document_type = RASTER_DOCUMENT_TYPE_DEVICE_9_16,
	  .keywords = _raster_document_type_device_9_16_keywords,
	  .color_space = RASTER_COLOR_SPACE_DEVICE_9,
	  .bits_per_pixel = 16,
	  .bits_per_color = 144,
	  .num_colors = 9
	},
	{ .document_type = RASTER_DOCUMENT_TYPE_DEVICE_10_16,
	  .keywords = _raster_document_type_device_10_16_keywords,
	  .color_space = RASTER_COLOR_SPACE_DEVICE_10,
	  .bits_per_pixel = 16,
	  .bits_per_color = 160,
	  .num_colors = 10
	},
	{ .document_type = RASTER_DOCUMENT_TYPE_DEVICE_11_16,
	  .keywords = _raster_document_type_device_11_16_keywords,
	  .color_space = RASTER_COLOR_SPACE_DEVICE_11,
	  .bits_per_pixel = 16,
	  .bits_per_color = 176,
	  .num_colors = 11
	},
	{ .document_type = RASTER_DOCUMENT_TYPE_DEVICE_12_16,
	  .keywords = _raster_document_type_device_12_16_keywords,
	  .color_space = RASTER_COLOR_SPACE_DEVICE_12,
	  .bits_per_pixel = 16,
	  .bits_per_color = 192,
	  .num_colors = 12
	},
	{ .document_type = RASTER_DOCUMENT_TYPE_DEVICE_13_16,
	  .keywords = _raster_document_type_device_13_16_keywords,
	  .color_space = RASTER_COLOR_SPACE_DEVICE_13,
	  .bits_per_pixel = 16,
	  .bits_per_color = 208,
	  .num_colors = 13
	},
	{ .document_type = RASTER_DOCUMENT_TYPE_DEVICE_14_16,
	  .keywords = _raster_document_type_device_14_16_keywords,
	  .color_space = RASTER_COLOR_SPACE_DEVICE_14,
	  .bits_per_pixel = 16,
	  .bits_per_color = 224,
	  .num_colors = 14
	},
	{ .document_type = RASTER_DOCUMENT_TYPE_DEVICE_15_16,
	  .keywords = _raster_document_type_device_15_16_keywords,
	  .color_space = RASTER_COLOR_SPACE_DEVICE_15,
	  .bits_per_pixel = 16,
	  .bits_per_color = 240,
	  .num_colors = 15
	},
	{ .document_type = RASTER_DOCUMENT_TYPE_RGB_16,
	  .keywords = _raster_document_type_rgb_16_keywords,
	  .color_space = RASTER_COLOR_SPACE_RGB,
	  .bits_per_pixel = 16,
	  .bits_per_color = 48,
	  .num_colors = 3
	},
	{ .document_type = RASTER_DOCUMENT_TYPE_SGRAY_16,
	  .keywords = _raster_document_type_sgray_16_keywords,
	  .color_space = RASTER_COLOR_SPACE_SW,
	  .bits_per_pixel = 16,
	  .bits_per_color = 16,
	  .num_colors = 1
	},
	{ .document_type = RASTER_DOCUMENT_TYPE_SRGB_16,
	  .keywords = _raster_document_type_srgb_16_keywords,
	  .color_space = RASTER_COLOR_SPACE_SRGB,
	  .bits_per_pixel = 16,
	  .bits_per_color = 48,
	  .num_colors = 3
	}
};

#define ENUMPARSER(EN, DEFAULT)						\
	raster_##EN##_t							\
	_raster_##EN##_from_keyword (uint8_t *kw)			\
	{								\
		int i, j;						\
		const struct keyword *sk;				\
		size_t l;						\
									\
		l = strlen (kw);					\
									\
		for (i = 0; _raster_##EN[i].keywords != NULL; i++) {	\
			for (j = 0;					\
			     _raster_##EN[i].keywords[j].kw != NULL;	\
			     j++) {					\
				sk = &_raster_##EN[i].keywords[j];	\
					if (l >= sk->min &&		\
						!strncasecmp (		\
							sk->kw,		\
							kw,		\
							l		\
							)		\
						) {			\
						return _raster_##EN[i].EN; \
					}				\
			}						\
		}							\
		return DEFAULT;						\
	}								\

