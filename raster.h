/*
 * Copyright 2012-2013 James Cloos
 *
 *
 * This file is part of LibRaster.
 *
 * LibRaster is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * LibRaster is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with LibRaster.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef RASTER_H
#define RASTER_H

#ifdef  __cplusplus
# define RASTER_BEGIN_DECLS  extern "C" {
# define RASTER_END_DECLS    }
#else
# define RASTER_BEGIN_DECLS
# define RASTER_END_DECLS
#endif

#ifndef raster_public
# if defined (_MSC_VER) && ! defined (RASTER_WIN32_STATIC_BUILD)
#  define raster_public __declspec(dllimport)
# else
#  define raster_public
# endif
#endif

RASTER_BEGIN_DECLS

#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>

#define RASTER_MAGIC_SIZE 4
#define RASTER_STRING_WIDTH 64
#define RASTER_MAX_VENDOR_DATA 1088
#define RASTER_PAGE_HEADER_SIZE 1796
#define RASTER_PWGRASTER_STRING "PwgRaster"

typedef struct _raster raster_t;

enum raster_color_order_enum {
	RASTER_COLOR_ORDER_CHUNKED = 0,
};

typedef enum raster_color_order_enum raster_color_order_t;

enum raster_return_enum {
	RASTER_OK = 0,
	RASTER_NO_CONTEXT = -1,
	RASTER_NO_OUTPUT = -2,
	RASTER_NO_PAGE_HEADER = -3,
	RASTER_NO_PIXBUF = -4,
	RASTER_BAD_ARG = -5,
	RASTER_COPY_FAILED = -6,
	RASTER_SHORT_WRITE = -7
};

typedef enum raster_return_enum raster_return_t;

enum raster_color_space_enum {
	RASTER_COLOR_SPACE_RGB       =  1,
	RASTER_COLOR_SPACE_K         =  3,
	RASTER_COLOR_SPACE_CMYK      =  6,
	RASTER_COLOR_SPACE_SW        = 18,
	RASTER_COLOR_SPACE_SRGB      = 19,
	RASTER_COLOR_SPACE_ADOBERGB  = 20,
	RASTER_COLOR_SPACE_DEVICE_1  = 48,
	RASTER_COLOR_SPACE_DEVICE_2  = 49,
	RASTER_COLOR_SPACE_DEVICE_3  = 50,
	RASTER_COLOR_SPACE_DEVICE_4  = 51,
	RASTER_COLOR_SPACE_DEVICE_5  = 52,
	RASTER_COLOR_SPACE_DEVICE_6  = 53,
	RASTER_COLOR_SPACE_DEVICE_7  = 54,
	RASTER_COLOR_SPACE_DEVICE_8  = 55,
	RASTER_COLOR_SPACE_DEVICE_9  = 56,
	RASTER_COLOR_SPACE_DEVICE_10 = 57,
	RASTER_COLOR_SPACE_DEVICE_11 = 58,
	RASTER_COLOR_SPACE_DEVICE_12 = 59,
	RASTER_COLOR_SPACE_DEVICE_13 = 60,
	RASTER_COLOR_SPACE_DEVICE_14 = 61,
	RASTER_COLOR_SPACE_DEVICE_15 = 62,
	RASTER_COLOR_SPACE_UNKNOWN = -1
};

typedef enum raster_color_space_enum raster_color_space_t;

enum raster_edge_enum {
	RASTER_EDGE_SHORT = 0,
	RASTER_EDGE_LONG = 1
};

typedef enum raster_edge_enum raster_edge_t;

enum raster_media_position_enum {
	RASTER_MEDIA_POSITION_AUTO = 0,
	RASTER_MEDIA_POSITION_MAIN = 1,
	RASTER_MEDIA_POSITION_ALTERNATE = 2,
	RASTER_MEDIA_POSITION_LARGE_CAPACITY = 3,
	RASTER_MEDIA_POSITION_MANUAL = 4,
	RASTER_MEDIA_POSITION_ENVELOPE = 5,
	RASTER_MEDIA_POSITION_DISC = 6,
	RASTER_MEDIA_POSITION_PHOTO = 7,
	RASTER_MEDIA_POSITION_HAGAKI = 8,
	RASTER_MEDIA_POSITION_MAIN_ROLL = 9,
	RASTER_MEDIA_POSITION_ALTERNATE_ROLL = 10,
	RASTER_MEDIA_POSITION_TOP = 11,
	RASTER_MEDIA_POSITION_MIDDLE = 12,
	RASTER_MEDIA_POSITION_BOTTOM = 13,
	RASTER_MEDIA_POSITION_SIDE = 14,
	RASTER_MEDIA_POSITION_LEFT = 15,
	RASTER_MEDIA_POSITION_RIGHT = 16,
	RASTER_MEDIA_POSITION_CENTER = 17,
	RASTER_MEDIA_POSITION_REAR = 18,
	RASTER_MEDIA_POSITION_BYPASS_TRAY = 19,
	RASTER_MEDIA_POSITION_TRAY_1 = 20,
	RASTER_MEDIA_POSITION_TRAY_2 = 21,
	RASTER_MEDIA_POSITION_TRAY_3 = 22,
	RASTER_MEDIA_POSITION_TRAY_4 = 23,
	RASTER_MEDIA_POSITION_TRAY_5 = 24,
	RASTER_MEDIA_POSITION_TRAY_6 = 25,
	RASTER_MEDIA_POSITION_TRAY_7 = 26,
	RASTER_MEDIA_POSITION_TRAY_8 = 27,
	RASTER_MEDIA_POSITION_TRAY_9 = 28,
	RASTER_MEDIA_POSITION_TRAY_10 = 29,
	RASTER_MEDIA_POSITION_TRAY_11 = 30,
	RASTER_MEDIA_POSITION_TRAY_12 = 31,
	RASTER_MEDIA_POSITION_TRAY_13 = 32,
	RASTER_MEDIA_POSITION_TRAY_14 = 33,
	RASTER_MEDIA_POSITION_TRAY_15 = 34,
	RASTER_MEDIA_POSITION_TRAY_16 = 35,
	RASTER_MEDIA_POSITION_TRAY_17 = 36,
	RASTER_MEDIA_POSITION_TRAY_18 = 37,
	RASTER_MEDIA_POSITION_TRAY_19 = 38,
	RASTER_MEDIA_POSITION_TRAY_20 = 39,
	RASTER_MEDIA_POSITION_ROLL_1 = 40,
	RASTER_MEDIA_POSITION_ROLL_2 = 41,
	RASTER_MEDIA_POSITION_ROLL_3 = 42,
	RASTER_MEDIA_POSITION_ROLL_4 = 43,
	RASTER_MEDIA_POSITION_ROLL_5 = 44,
	RASTER_MEDIA_POSITION_ROLL_6 = 45,
	RASTER_MEDIA_POSITION_ROLL_7 = 46,
	RASTER_MEDIA_POSITION_ROLL_8 = 47,
	RASTER_MEDIA_POSITION_ROLL_9 = 48,
	RASTER_MEDIA_POSITION_ROLL_10 = 49
};

typedef enum raster_media_position_enum raster_media_position_t;

enum raster_orientation_enum {
	RASTER_ORIENTATION_PORTRAIT = 0,
	RASTER_ORIENTATION_LANDSCAPE = 1,
	RASTER_ORIENTATION_REVERSE_PORTRAIT = 2,
	RASTER_ORIENTATION_REVERSE_LANDSCAPE = 3,
	RASTER_ORIENTATION_MAX
};

typedef enum raster_orientation_enum raster_orientation_t;

enum raster_print_quality_enum {
	RASTER_QUALITY_DEFAULT = 0,
	RASTER_QUALITY_DRAFT = 3,
	RASTER_QUALITY_NORMAL = 4,
	RASTER_QUALITY_HIGH = 5
};

typedef enum raster_print_quality_enum raster_print_quality_t;

enum raster_when_enum {
	RASTER_WHEN_NEVER = 0,
	RASTER_WHEN_AFTER_DOCUMENT = 1,
	RASTER_WHEN_AFTER_JOB = 2,
	RASTER_WHEN_AFTER_SET = 3,
	RASTER_WHEN_AFTER_PAGE = 4
};

typedef enum raster_when_enum raster_when_t;

enum raster_xform_enum {
	RASTER_XFORM_REVERSED = -1,
	RASTER_XFORM_UNKNOWN = 0,
	RASTER_XFORM_NORMAL = 1
};

typedef enum raster_xform_enum raster_xform_t;

enum raster_sides_enum {
	RASTER_SIDES_ONE_SIDED,
	RASTER_SIDES_TWO_SIDED_LONG_EDGE,
	RASTER_SIDES_TWO_SIDED_SHORT_EDGE
};

typedef enum raster_sides_enum raster_sides_t;

enum raster_sheetback_enum {
	RASTER_SHEETBACK_NORMAL,
	RASTER_SHEETBACK_FLIPPED,
	RASTER_SHEETBACK_ROTATED,
	RASTER_SHEETBACK_MANUAL
};

typedef enum raster_sheetback_enum raster_sheetback_t;

enum raster_document_type_enum {
	RASTER_DOCUMENT_TYPE_UNKNOWN = -1,
	RASTER_DOCUMENT_TYPE_BLACK_1,
	RASTER_DOCUMENT_TYPE_SGRAY_1,
	RASTER_DOCUMENT_TYPE_ADOBE_RGB_8,
	RASTER_DOCUMENT_TYPE_BLACK_8,
	RASTER_DOCUMENT_TYPE_CMYK_8,
	RASTER_DOCUMENT_TYPE_DEVICE_1_8,
	RASTER_DOCUMENT_TYPE_DEVICE_2_8,
	RASTER_DOCUMENT_TYPE_DEVICE_3_8,
	RASTER_DOCUMENT_TYPE_DEVICE_4_8,
	RASTER_DOCUMENT_TYPE_DEVICE_5_8,
	RASTER_DOCUMENT_TYPE_DEVICE_6_8,
	RASTER_DOCUMENT_TYPE_DEVICE_7_8,
	RASTER_DOCUMENT_TYPE_DEVICE_8_8,
	RASTER_DOCUMENT_TYPE_DEVICE_9_8,
	RASTER_DOCUMENT_TYPE_DEVICE_10_8,
	RASTER_DOCUMENT_TYPE_DEVICE_11_8,
	RASTER_DOCUMENT_TYPE_DEVICE_12_8,
	RASTER_DOCUMENT_TYPE_DEVICE_13_8,
	RASTER_DOCUMENT_TYPE_DEVICE_14_8,
	RASTER_DOCUMENT_TYPE_DEVICE_15_8,
	RASTER_DOCUMENT_TYPE_RGB_8,
	RASTER_DOCUMENT_TYPE_SGRAY_8,
	RASTER_DOCUMENT_TYPE_SRGB_8,
	RASTER_DOCUMENT_TYPE_ADOBE_RGB_16,
	RASTER_DOCUMENT_TYPE_BLACK_16,
	RASTER_DOCUMENT_TYPE_CMYK_16,
	RASTER_DOCUMENT_TYPE_DEVICE_1_16,
	RASTER_DOCUMENT_TYPE_DEVICE_2_16,
	RASTER_DOCUMENT_TYPE_DEVICE_3_16,
	RASTER_DOCUMENT_TYPE_DEVICE_4_16,
	RASTER_DOCUMENT_TYPE_DEVICE_5_16,
	RASTER_DOCUMENT_TYPE_DEVICE_6_16,
	RASTER_DOCUMENT_TYPE_DEVICE_7_16,
	RASTER_DOCUMENT_TYPE_DEVICE_8_16,
	RASTER_DOCUMENT_TYPE_DEVICE_9_16,
	RASTER_DOCUMENT_TYPE_DEVICE_10_16,
	RASTER_DOCUMENT_TYPE_DEVICE_11_16,
	RASTER_DOCUMENT_TYPE_DEVICE_12_16,
	RASTER_DOCUMENT_TYPE_DEVICE_13_16,
	RASTER_DOCUMENT_TYPE_DEVICE_14_16,
	RASTER_DOCUMENT_TYPE_DEVICE_15_16,
	RASTER_DOCUMENT_TYPE_RGB_16,
	RASTER_DOCUMENT_TYPE_SGRAY_16,
	RASTER_DOCUMENT_TYPE_SRGB_16,
	RASTER_DOCUMENT_TYPE_MAX
};

typedef enum raster_document_type_enum raster_document_type_t;

raster_public raster_t *
raster_create (void);

raster_public raster_t *
raster_reference (raster_t *raster);

raster_public void
raster_destroy (raster_t *raster);

raster_public int
raster_set_output (raster_t *raster, FILE *output);

raster_public int
raster_set_document_type (raster_t *raster, raster_document_type_t dt);

raster_public int
raster_set_media_color (raster_t *raster, const uint8_t *media_color, size_t len);

raster_public int
raster_set_vendor_id (raster_t *raster, uint32_t vendor_id);

raster_public int
raster_set_vendor_data (raster_t *raster, const uint8_t *vendor_data, size_t len);

raster_public int
raster_set_media_type (raster_t *raster, const uint8_t *media_type, size_t len);

raster_public int
raster_set_page_size_name (raster_t *raster, const uint8_t *page_size_name, size_t len);

raster_public int
raster_set_print_content_optimize (raster_t *raster, const uint8_t *print_content_optimize, size_t len);

raster_public int
raster_set_rendering_intent (raster_t *raster, const uint8_t *rendering_intent, size_t len);

raster_public int
raster_set_color_order (raster_t *raster, raster_color_order_t color_order);

raster_public int
raster_set_color_space (raster_t *raster, raster_color_space_t color_space);

raster_public int
raster_set_leading_edge (raster_t *raster, raster_edge_t leading_edge);

raster_public int
raster_set_media_position (raster_t *raster, raster_media_position_t media_position);

raster_public int
raster_set_orientation (raster_t *raster, raster_orientation_t orientation);

raster_public int
raster_set_print_quality (raster_t *raster, raster_print_quality_t print_quality);

raster_public int
raster_set_cut_media (raster_t *raster, raster_when_t cut_media);

raster_public int
raster_set_jog (raster_t *raster, raster_when_t jog);

raster_public int
raster_set_cross_feed_xform (raster_t *raster, raster_xform_t cross_feed_xform);

raster_public int
raster_set_feed_xform (raster_t *raster, raster_xform_t feed_xform);

raster_public int
raster_set_alternate_primary (raster_t *raster, uint8_t alt_red, uint8_t alt_green, uint8_t alt_blue);

raster_public int
raster_set_bits_per_color (raster_t *raster, uint32_t bits_per_color);

raster_public int
raster_set_bits_per_pixel (raster_t *raster, uint32_t bits_per_pixel);

raster_public int
raster_set_page_pixel_size (raster_t *raster, uint32_t cross_pixel_size, uint32_t feed_pixel_size);

raster_public int
raster_set_resolution_dpi (raster_t *raster, uint32_t cross_resolution, uint32_t feed_resolution);

raster_public int
raster_set_page_point_size (raster_t *raster, uint32_t cross_point_size, uint32_t feed_point_size);

raster_public int
raster_set_bbox (raster_t *raster, uint32_t left, uint32_t right, uint32_t top, uint32_t bottom);

raster_public int
raster_set_media_weight_metric (raster_t *raster, uint32_t media_weight_metric);

raster_public int
raster_set_num_colors (raster_t *raster, uint32_t num_colors);

raster_public int
raster_set_num_copies (raster_t *raster, uint32_t num_copies);

raster_public int
raster_set_duplex (raster_t *raster, bool duplex);

raster_public int
raster_set_insert_sheet (raster_t *raster, bool insert_sheet);

raster_public int
raster_set_tumble (raster_t *raster, bool tumble);

raster_public int
raster_set_sides_xform (raster_t *raster, raster_sides_t sides, raster_sheetback_t back);

raster_public int
raster_write_magic (raster_t *raster);

raster_public int
raster_write_page (raster_t *raster, uint8_t *pixmap, size_t len);

raster_public int
raster_get_media_color (const raster_t *raster, uint8_t *media_color, size_t len);

raster_public int
raster_get_vendor_id (const raster_t *raster, uint32_t *vendor_id);

raster_public int
raster_get_vendor_data (const raster_t *raster, uint8_t *vendor_data, size_t len);

raster_public int
raster_get_media_type (const raster_t *raster, uint8_t *media_type, size_t len);

raster_public int
raster_get_page_size_name (const raster_t *raster, uint8_t *page_size_name, size_t len);

raster_public int
raster_get_print_content_optimize (const raster_t *raster, uint8_t *print_content_optimize, size_t len);

raster_public int
raster_get_rendering_intent (const raster_t *raster, uint8_t *rendering_intent, size_t len);

raster_public int
raster_get_color_order (const raster_t *raster, raster_color_order_t *color_order);

raster_public int
raster_get_color_space (const raster_t *raster, raster_color_space_t *color_space);

raster_public int
raster_get_leading_edge (const raster_t *raster, raster_edge_t *leading_edge);

raster_public int
raster_get_media_position (const raster_t *raster, raster_media_position_t *media_position);

raster_public int
raster_get_orientation (const raster_t *raster, raster_orientation_t *orientation);

raster_public int
raster_get_print_quality (const raster_t *raster, raster_print_quality_t *print_quality);

raster_public int
raster_get_cut_media (const raster_t *raster, raster_when_t *cut_media);

raster_public int
raster_get_jog (const raster_t *raster, raster_when_t *jog);

raster_public int
raster_get_cross_feed_xform (const raster_t *raster, raster_xform_t *cross_xform, raster_xform_t *feed_xform);

raster_public int
raster_get_alternate_primary (const raster_t *raster, uint8_t *alt_red, uint8_t *alt_green, uint8_t *alt_blue);

raster_public int
raster_get_bits_per_color (const raster_t *raster, uint32_t *bits_per_color);

raster_public int
raster_get_bits_per_pixel (const raster_t *raster, uint32_t *bits_per_pixel);

raster_public int
raster_get_page_pixel_size (const raster_t *raster, uint32_t *cross_pixels, uint32_t *feed_pixels);

raster_public int
raster_get_resolution_dpi (const raster_t *raster, uint32_t *cross_dpi, uint32_t *feed_dpi);

raster_public int
raster_get_page_point_size (const raster_t *raster, uint32_t *cross_points, uint32_t *feed_points);

raster_public int
raster_get_bbox (const raster_t *raster, uint32_t *left, uint32_t *right, uint32_t *top, uint32_t *bottom);

raster_public int
raster_get_media_weight_metric (const raster_t *raster, uint32_t *media_weight_metric);

raster_public int
raster_get_num_colors (const raster_t *raster, uint32_t *num_colors);

raster_public int
raster_get_num_copies (const raster_t *raster, uint32_t *num_copies);

raster_public int
raster_get_duplex (const raster_t *raster, bool *duplex);

raster_public int
raster_get_insert_sheet (const raster_t *raster, bool *insert_sheet);

raster_public int
raster_get_tumble (const raster_t *raster, bool *tumble);

raster_public char *
raster_get_error_string (const raster_t *raster);

raster_public int
raster_clear_error_string (raster_t *raster);

raster_public size_t
raster_get_octets_written (const raster_t *raster);

raster_public int
raster_clear_octets_written (raster_t *raster);

RASTER_END_DECLS

#endif
