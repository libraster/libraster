/*
 * Copyright 2012-2013 James Cloos
 *
 *
 * This file is part of LibRaster.
 *
 * LibRaster is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * LibRaster is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with LibRaster.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef RASTER_PRIVATE_H
#define RASTER_PRIVATE_H

RASTER_BEGIN_DECLS

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <string.h>
#include <arpa/inet.h>

#define likely(x)    __builtin_expect(!!(x), 1)
#define unlikely(x)  __builtin_expect(!!(x), 0)

struct raster_page_header {
	char media_color[RASTER_STRING_WIDTH];
	char media_type[RASTER_STRING_WIDTH];
	char page_size_name[RASTER_STRING_WIDTH];
	char print_content_optimize[RASTER_STRING_WIDTH];
	char rendering_intent[RASTER_STRING_WIDTH];

	raster_color_order_t color_order;
	raster_color_space_t color_space;
	raster_edge_t leading_edge;
	raster_media_position_t media_position;
	raster_orientation_t orientation;
	raster_print_quality_t print_quality;
	raster_when_t cut_media;
	raster_when_t jog;

	int32_t cross_feed_xform;
	int32_t feed_xform;

	uint32_t alternate_primary;
	uint32_t bits_per_color;
	uint32_t bits_per_pixel;
	uint32_t cross_point_size;
	uint32_t cross_resolution;
	uint32_t feed_point_size;
	uint32_t feed_resolution;
	uint32_t height; /*px*/
	uint32_t image_bbox_bottom;
	uint32_t image_bbox_left;
	uint32_t image_bbox_right;
	uint32_t image_bbox_top;
	uint32_t media_weight_metric;
	uint32_t num_colors;
	uint32_t num_copies;
	uint32_t total_page_count;
	uint32_t vendor_identifier;
	uint32_t width; /*px*/

	bool duplex;
	bool insert_sheet;
	bool tumble;

	size_t vendor_data_length;
	char  *vendor_data;
};

struct _raster {
	struct raster_page_header *header;
	FILE  *output;
	FILE  *input;
	char  *errorstring;
	size_t octets_in;
	size_t octets_out;
	int    references;
	int    is_ok;
};

typedef struct _raster raster_t;

RASTER_END_DECLS

#endif /* RASTER_PRIVATE_H */
